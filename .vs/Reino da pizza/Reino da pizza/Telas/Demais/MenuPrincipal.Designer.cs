﻿namespace Reino_da_pizza
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuPrincipal));
            this.label10 = new System.Windows.Forms.Label();
            this.ptbFornecedor = new System.Windows.Forms.PictureBox();
            this.CMSFornecedor = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.lblus = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.CMSProduto = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.produtoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.pToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produçãoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ptbProduto = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.CMSFuncionario = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.CMSCompraVenda = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.compraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compraToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendaToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.compraToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ptbFolhaPagamento = new System.Windows.Forms.PictureBox();
            this.CMSFolhaPagamento = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.fluxoDeCaixaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folhaDePagamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.ptbCliente = new System.Windows.Forms.PictureBox();
            this.CMSCliente = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.Deslogar = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.gastosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.ptbFornecedor)).BeginInit();
            this.CMSFornecedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.CMSProduto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.CMSFuncionario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.CMSCompraVenda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbFolhaPagamento)).BeginInit();
            this.CMSFolhaPagamento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCliente)).BeginInit();
            this.CMSCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.Deslogar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Gold;
            this.label10.Location = new System.Drawing.Point(615, 318);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 16);
            this.label10.TabIndex = 229;
            this.label10.Text = "Fornecedor";
            // 
            // ptbFornecedor
            // 
            this.ptbFornecedor.BackColor = System.Drawing.Color.Transparent;
            this.ptbFornecedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbFornecedor.BackgroundImage")));
            this.ptbFornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbFornecedor.ContextMenuStrip = this.CMSFornecedor;
            this.ptbFornecedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptbFornecedor.Location = new System.Drawing.Point(630, 244);
            this.ptbFornecedor.Name = "ptbFornecedor";
            this.ptbFornecedor.Size = new System.Drawing.Size(98, 71);
            this.ptbFornecedor.TabIndex = 228;
            this.ptbFornecedor.TabStop = false;
            // 
            // CMSFornecedor
            // 
            this.CMSFornecedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSFornecedor.BackgroundImage")));
            this.CMSFornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSFornecedor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.CMSFornecedor.Name = "CMSProduto";
            this.CMSFornecedor.Size = new System.Drawing.Size(126, 48);
            this.CMSFornecedor.Opening += new System.ComponentModel.CancelEventHandler(this.CMSFornecedor_Opening);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem5.Image")));
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem5.Text = "Novo";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem6.Image")));
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem6.Text = "Consultar";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // lblus
            // 
            this.lblus.AutoSize = true;
            this.lblus.BackColor = System.Drawing.Color.Maroon;
            this.lblus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblus.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblus.ForeColor = System.Drawing.Color.Gold;
            this.lblus.Location = new System.Drawing.Point(68, 33);
            this.lblus.Name = "lblus";
            this.lblus.Size = new System.Drawing.Size(80, 18);
            this.lblus.TabIndex = 227;
            this.lblus.Text = "Usuário";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Maroon;
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Location = new System.Drawing.Point(12, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(55, 50);
            this.pictureBox3.TabIndex = 226;
            this.pictureBox3.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ContextMenuStrip = this.CMSProduto;
            this.label6.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Gold;
            this.label6.Location = new System.Drawing.Point(552, 433);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(176, 16);
            this.label6.TabIndex = 225;
            this.label6.Text = "Produto/Produção";
            // 
            // CMSProduto
            // 
            this.CMSProduto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSProduto.BackgroundImage")));
            this.CMSProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSProduto.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.CMSProduto.Name = "CMSCliente";
            this.CMSProduto.Size = new System.Drawing.Size(126, 48);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.produtoToolStripMenuItem,
            this.produçãoToolStripMenuItem});
            this.toolStripMenuItem3.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem3.Image")));
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem3.Text = "Novo";
            // 
            // produtoToolStripMenuItem
            // 
            this.produtoToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("produtoToolStripMenuItem.BackgroundImage")));
            this.produtoToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.produtoToolStripMenuItem.ForeColor = System.Drawing.Color.Orange;
            this.produtoToolStripMenuItem.Name = "produtoToolStripMenuItem";
            this.produtoToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.produtoToolStripMenuItem.Text = "Produto";
            this.produtoToolStripMenuItem.Click += new System.EventHandler(this.produtoToolStripMenuItem_Click);
            // 
            // produçãoToolStripMenuItem
            // 
            this.produçãoToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("produçãoToolStripMenuItem.BackgroundImage")));
            this.produçãoToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.produçãoToolStripMenuItem.ForeColor = System.Drawing.Color.Orange;
            this.produçãoToolStripMenuItem.Name = "produçãoToolStripMenuItem";
            this.produçãoToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.produçãoToolStripMenuItem.Text = "Produção";
            this.produçãoToolStripMenuItem.Click += new System.EventHandler(this.produçãoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pToolStripMenuItem,
            this.produçãoToolStripMenuItem1});
            this.toolStripMenuItem4.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem4.Image")));
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem4.Text = "Consultar";
            // 
            // pToolStripMenuItem
            // 
            this.pToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pToolStripMenuItem.BackgroundImage")));
            this.pToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pToolStripMenuItem.ForeColor = System.Drawing.Color.Orange;
            this.pToolStripMenuItem.Name = "pToolStripMenuItem";
            this.pToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.pToolStripMenuItem.Text = "Produto";
            this.pToolStripMenuItem.Click += new System.EventHandler(this.pToolStripMenuItem_Click);
            // 
            // produçãoToolStripMenuItem1
            // 
            this.produçãoToolStripMenuItem1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("produçãoToolStripMenuItem1.BackgroundImage")));
            this.produçãoToolStripMenuItem1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.produçãoToolStripMenuItem1.ForeColor = System.Drawing.Color.Orange;
            this.produçãoToolStripMenuItem1.Name = "produçãoToolStripMenuItem1";
            this.produçãoToolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.produçãoToolStripMenuItem1.Text = "Produção";
            this.produçãoToolStripMenuItem1.Click += new System.EventHandler(this.produçãoToolStripMenuItem1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gold;
            this.label5.Location = new System.Drawing.Point(33, 318);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 224;
            this.label5.Text = "Finanças";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gold;
            this.label4.Location = new System.Drawing.Point(29, 440);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 16);
            this.label4.TabIndex = 223;
            this.label4.Text = "Venda/Compra";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gold;
            this.label3.Location = new System.Drawing.Point(623, 210);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 16);
            this.label3.TabIndex = 222;
            this.label3.Text = "Funcionário";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gold;
            this.label2.Location = new System.Drawing.Point(33, 210);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 16);
            this.label2.TabIndex = 221;
            this.label2.Text = "Cliente";
            // 
            // ptbProduto
            // 
            this.ptbProduto.BackColor = System.Drawing.Color.Transparent;
            this.ptbProduto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbProduto.BackgroundImage")));
            this.ptbProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbProduto.ContextMenuStrip = this.CMSProduto;
            this.ptbProduto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptbProduto.Location = new System.Drawing.Point(630, 359);
            this.ptbProduto.Name = "ptbProduto";
            this.ptbProduto.Size = new System.Drawing.Size(98, 71);
            this.ptbProduto.TabIndex = 220;
            this.ptbProduto.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox11.BackgroundImage")));
            this.pictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox11.ContextMenuStrip = this.CMSFuncionario;
            this.pictureBox11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox11.Location = new System.Drawing.Point(628, 136);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(100, 71);
            this.pictureBox11.TabIndex = 219;
            this.pictureBox11.TabStop = false;
            // 
            // CMSFuncionario
            // 
            this.CMSFuncionario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSFuncionario.BackgroundImage")));
            this.CMSFuncionario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSFuncionario.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem13,
            this.toolStripMenuItem14});
            this.CMSFuncionario.Name = "CMSProduto";
            this.CMSFuncionario.Size = new System.Drawing.Size(126, 48);
            this.CMSFuncionario.Opening += new System.ComponentModel.CancelEventHandler(this.CMSFuncionario_Opening);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem13.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem13.Image")));
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem13.Text = "Novo";
            this.toolStripMenuItem13.Click += new System.EventHandler(this.toolStripMenuItem13_Click);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem14.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem14.Image")));
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem14.Text = "Consultar";
            this.toolStripMenuItem14.Click += new System.EventHandler(this.toolStripMenuItem14_Click);
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox10.BackgroundImage")));
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox10.ContextMenuStrip = this.CMSCompraVenda;
            this.pictureBox10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox10.Location = new System.Drawing.Point(36, 359);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(98, 71);
            this.pictureBox10.TabIndex = 218;
            this.pictureBox10.TabStop = false;
            // 
            // CMSCompraVenda
            // 
            this.CMSCompraVenda.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSCompraVenda.BackgroundImage")));
            this.CMSCompraVenda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSCompraVenda.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem11,
            this.toolStripMenuItem12});
            this.CMSCompraVenda.Name = "CMSProduto";
            this.CMSCompraVenda.Size = new System.Drawing.Size(126, 48);
            this.CMSCompraVenda.Opening += new System.ComponentModel.CancelEventHandler(this.CMSPedidoVenda_Opening);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compraToolStripMenuItem});
            this.toolStripMenuItem11.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem11.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem11.Image")));
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem11.Text = "Novo";
            // 
            // compraToolStripMenuItem
            // 
            this.compraToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("compraToolStripMenuItem.BackgroundImage")));
            this.compraToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.compraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compraToolStripMenuItem2,
            this.vendaToolStripMenuItem2});
            this.compraToolStripMenuItem.ForeColor = System.Drawing.Color.Orange;
            this.compraToolStripMenuItem.Name = "compraToolStripMenuItem";
            this.compraToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.compraToolStripMenuItem.Text = "Pedido";
            // 
            // compraToolStripMenuItem2
            // 
            this.compraToolStripMenuItem2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("compraToolStripMenuItem2.BackgroundImage")));
            this.compraToolStripMenuItem2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.compraToolStripMenuItem2.ForeColor = System.Drawing.Color.Orange;
            this.compraToolStripMenuItem2.Name = "compraToolStripMenuItem2";
            this.compraToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.compraToolStripMenuItem2.Text = "Compra";
            this.compraToolStripMenuItem2.Click += new System.EventHandler(this.compraToolStripMenuItem2_Click);
            // 
            // vendaToolStripMenuItem2
            // 
            this.vendaToolStripMenuItem2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vendaToolStripMenuItem2.BackgroundImage")));
            this.vendaToolStripMenuItem2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vendaToolStripMenuItem2.ForeColor = System.Drawing.Color.Orange;
            this.vendaToolStripMenuItem2.Name = "vendaToolStripMenuItem2";
            this.vendaToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.vendaToolStripMenuItem2.Text = "Venda";
            this.vendaToolStripMenuItem2.Click += new System.EventHandler(this.vendaToolStripMenuItem2_Click);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compraToolStripMenuItem1,
            this.vendaToolStripMenuItem1});
            this.toolStripMenuItem12.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem12.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem12.Image")));
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem12.Text = "Consultar";
            // 
            // compraToolStripMenuItem1
            // 
            this.compraToolStripMenuItem1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("compraToolStripMenuItem1.BackgroundImage")));
            this.compraToolStripMenuItem1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.compraToolStripMenuItem1.ForeColor = System.Drawing.Color.Orange;
            this.compraToolStripMenuItem1.Name = "compraToolStripMenuItem1";
            this.compraToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.compraToolStripMenuItem1.Text = "Compra";
            this.compraToolStripMenuItem1.Click += new System.EventHandler(this.compraToolStripMenuItem1_Click);
            // 
            // vendaToolStripMenuItem1
            // 
            this.vendaToolStripMenuItem1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vendaToolStripMenuItem1.BackgroundImage")));
            this.vendaToolStripMenuItem1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vendaToolStripMenuItem1.ForeColor = System.Drawing.Color.Orange;
            this.vendaToolStripMenuItem1.Name = "vendaToolStripMenuItem1";
            this.vendaToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.vendaToolStripMenuItem1.Text = "Venda";
            this.vendaToolStripMenuItem1.Click += new System.EventHandler(this.vendaToolStripMenuItem1_Click);
            // 
            // ptbFolhaPagamento
            // 
            this.ptbFolhaPagamento.BackColor = System.Drawing.Color.Transparent;
            this.ptbFolhaPagamento.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbFolhaPagamento.BackgroundImage")));
            this.ptbFolhaPagamento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbFolhaPagamento.ContextMenuStrip = this.CMSFolhaPagamento;
            this.ptbFolhaPagamento.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptbFolhaPagamento.Location = new System.Drawing.Point(34, 244);
            this.ptbFolhaPagamento.Name = "ptbFolhaPagamento";
            this.ptbFolhaPagamento.Size = new System.Drawing.Size(98, 71);
            this.ptbFolhaPagamento.TabIndex = 217;
            this.ptbFolhaPagamento.TabStop = false;
            // 
            // CMSFolhaPagamento
            // 
            this.CMSFolhaPagamento.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSFolhaPagamento.BackgroundImage")));
            this.CMSFolhaPagamento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSFolhaPagamento.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem9,
            this.toolStripMenuItem10});
            this.CMSFolhaPagamento.Name = "CMSProduto";
            this.CMSFolhaPagamento.Size = new System.Drawing.Size(153, 70);
            this.CMSFolhaPagamento.Opening += new System.ComponentModel.CancelEventHandler(this.CMSFolhaPagamento_Opening_1);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fluxoDeCaixaToolStripMenuItem,
            this.folhaDePagamentoToolStripMenuItem,
            this.gastosToolStripMenuItem});
            this.toolStripMenuItem9.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem9.Image")));
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItem9.Text = "Novo";
            this.toolStripMenuItem9.Click += new System.EventHandler(this.toolStripMenuItem9_Click);
            // 
            // fluxoDeCaixaToolStripMenuItem
            // 
            this.fluxoDeCaixaToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fluxoDeCaixaToolStripMenuItem.BackgroundImage")));
            this.fluxoDeCaixaToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fluxoDeCaixaToolStripMenuItem.ForeColor = System.Drawing.Color.Orange;
            this.fluxoDeCaixaToolStripMenuItem.Name = "fluxoDeCaixaToolStripMenuItem";
            this.fluxoDeCaixaToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.fluxoDeCaixaToolStripMenuItem.Text = "Fluxo De Caixa";
            this.fluxoDeCaixaToolStripMenuItem.Click += new System.EventHandler(this.fluxoDeCaixaToolStripMenuItem_Click);
            // 
            // folhaDePagamentoToolStripMenuItem
            // 
            this.folhaDePagamentoToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("folhaDePagamentoToolStripMenuItem.BackgroundImage")));
            this.folhaDePagamentoToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.folhaDePagamentoToolStripMenuItem.ForeColor = System.Drawing.Color.Orange;
            this.folhaDePagamentoToolStripMenuItem.Name = "folhaDePagamentoToolStripMenuItem";
            this.folhaDePagamentoToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.folhaDePagamentoToolStripMenuItem.Text = "Folha De Pagamento";
            this.folhaDePagamentoToolStripMenuItem.Click += new System.EventHandler(this.folhaDePagamentoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem10.Image")));
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem10.Text = "Consultar";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
            // 
            // ptbCliente
            // 
            this.ptbCliente.BackColor = System.Drawing.Color.Transparent;
            this.ptbCliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbCliente.BackgroundImage")));
            this.ptbCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbCliente.ContextMenuStrip = this.CMSCliente;
            this.ptbCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptbCliente.Location = new System.Drawing.Point(32, 136);
            this.ptbCliente.Name = "ptbCliente";
            this.ptbCliente.Size = new System.Drawing.Size(100, 71);
            this.ptbCliente.TabIndex = 216;
            this.ptbCliente.TabStop = false;
            // 
            // CMSCliente
            // 
            this.CMSCliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSCliente.BackgroundImage")));
            this.CMSCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSCliente.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem7,
            this.toolStripMenuItem8});
            this.CMSCliente.Name = "CMSProduto";
            this.CMSCliente.Size = new System.Drawing.Size(126, 48);
            this.CMSCliente.Opening += new System.ComponentModel.CancelEventHandler(this.CMSCliente_Opening);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem7.Image")));
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem7.Text = "Novo";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.toolStripMenuItem7_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem8.Image")));
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem8.Text = "Consultar";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Orange;
            this.pictureBox2.Location = new System.Drawing.Point(-10, 65);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(810, 13);
            this.pictureBox2.TabIndex = 244;
            this.pictureBox2.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Maroon;
            this.label8.Font = new System.Drawing.Font("Copperplate Gothic Bold", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Gold;
            this.label8.Location = new System.Drawing.Point(321, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(132, 41);
            this.label8.TabIndex = 245;
            this.label8.Text = "Menu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Maroon;
            this.label1.Font = new System.Drawing.Font("Copperplate Gothic Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gold;
            this.label1.Location = new System.Drawing.Point(687, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 30);
            this.label1.TabIndex = 250;
            this.label1.Text = "_";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Maroon;
            this.pictureBox1.Location = new System.Drawing.Point(-5, -10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(805, 76);
            this.pictureBox1.TabIndex = 246;
            this.pictureBox1.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Maroon;
            this.label7.Font = new System.Drawing.Font("Copperplate Gothic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Gold;
            this.label7.Location = new System.Drawing.Point(710, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 26);
            this.label7.TabIndex = 249;
            this.label7.Text = "X";
            this.label7.Click += new System.EventHandler(this.label7_Click_1);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("contextMenuStrip1.BackgroundImage")));
            this.contextMenuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.contextMenuStrip1.Name = "CMSCliente";
            this.contextMenuStrip1.Size = new System.Drawing.Size(126, 48);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem1.Text = "Novo";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.Image")));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem2.Text = "Consultar";
            // 
            // Deslogar
            // 
            this.Deslogar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Deslogar.BackgroundImage")));
            this.Deslogar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Deslogar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem15});
            this.Deslogar.Name = "CMSCliente";
            this.Deslogar.Size = new System.Drawing.Size(121, 26);
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.ForeColor = System.Drawing.Color.Orange;
            this.toolStripMenuItem15.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem15.Image")));
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(120, 22);
            this.toolStripMenuItem15.Text = "Deslogar";
            this.toolStripMenuItem15.Click += new System.EventHandler(this.toolStripMenuItem15_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F);
            this.label9.ForeColor = System.Drawing.Color.Gold;
            this.label9.Location = new System.Drawing.Point(335, 433);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 16);
            this.label9.TabIndex = 252;
            this.label9.Text = "Twitter";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Location = new System.Drawing.Point(328, 351);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(91, 79);
            this.pictureBox4.TabIndex = 251;
            this.pictureBox4.TabStop = false;
            // 
            // gastosToolStripMenuItem
            // 
            this.gastosToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gastosToolStripMenuItem.BackgroundImage")));
            this.gastosToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gastosToolStripMenuItem.ForeColor = System.Drawing.Color.Orange;
            this.gastosToolStripMenuItem.Name = "gastosToolStripMenuItem";
            this.gastosToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.gastosToolStripMenuItem.Text = "Gastos";
            this.gastosToolStripMenuItem.Click += new System.EventHandler(this.gastosToolStripMenuItem_Click);
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(746, 517);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblus);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.ptbFornecedor);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ptbProduto);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.ptbFolhaPagamento);
            this.Controls.Add(this.ptbCliente);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuPrincipal";
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.Menu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ptbFornecedor)).EndInit();
            this.CMSFornecedor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.CMSProduto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptbProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.CMSFuncionario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.CMSCompraVenda.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptbFolhaPagamento)).EndInit();
            this.CMSFolhaPagamento.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptbCliente)).EndInit();
            this.CMSCliente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.Deslogar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox ptbFornecedor;
        private System.Windows.Forms.Label lblus;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox ptbProduto;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox ptbFolhaPagamento;
        private System.Windows.Forms.PictureBox ptbCliente;
        private System.Windows.Forms.ContextMenuStrip CMSFornecedor;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ContextMenuStrip CMSCliente;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ContextMenuStrip CMSFolhaPagamento;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ContextMenuStrip CMSCompraVenda;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ContextMenuStrip CMSFuncionario;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ContextMenuStrip CMSProduto;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ContextMenuStrip Deslogar;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem produtoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produçãoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem compraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compraToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vendaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fluxoDeCaixaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folhaDePagamentoToolStripMenuItem;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.ToolStripMenuItem compraToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem vendaToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem gastosToolStripMenuItem;
    }
}