﻿using Reino_da_pizza.DB.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try { 
            FuncionarioBusiness business = new FuncionarioBusiness();
            FuncionarioDTO funcionario = business.Logar(txtUsuario.Text, txtSenha.Text);

            if (funcionario != null)
            {
                UserSession.UsuarioLogado = funcionario;

                MenuPrincipal menu = new MenuPrincipal();
                menu.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Credenciais inválidas.", "Reino da pizza", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            }
             catch(Exception ex)
             {
                 EnviarMensagemErro("Não foi possivel logar: " + ex.Message);
             }
        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label17_Click_1(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }
    }
}
