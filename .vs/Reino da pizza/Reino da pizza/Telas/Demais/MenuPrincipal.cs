﻿using Reino_da_pizza.DB.Funcionário;
using Reino_da_pizza.Telas;
using Reino_da_pizza.Telas.Compra;
using Reino_da_pizza.Telas.Venda;
using Reino_da_pizza.Telas.Venda.Produção;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();
            VerificarPermissoes();
            MudarNome();
        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }

        private void CMSPedidoVenda_Opening(object sender, CancelEventArgs e)
        {

        }
        private void MudarNome()
        {
            string usuario = UserSession.UsuarioLogado.Usuario;
            lblus.Text = usuario;
        }

        void VerificarPermissoes()
        {
            if (UserSession.UsuarioLogado.Admin == false)
            {
                if (UserSession.UsuarioLogado.RH == true)
                {
                    CMSFuncionario.Enabled = true;
                }

                if (UserSession.UsuarioLogado.Logistica == true)
                {
                    CMSCliente.Enabled = true;
                }
                if (UserSession.UsuarioLogado.Financeiro == true)
                {
                    CMSFolhaPagamento.Enabled = true;
                    CMSFornecedor.Enabled = true;
                    CMSFolhaPagamento.Enabled = true;
                }
                if (UserSession.UsuarioLogado.Vendas == true)
                {
                    CMSCliente.Enabled = true;
                    CMSProduto.Enabled = true;
                    CMSCompraVenda.Enabled = true;
                }
                if (UserSession.UsuarioLogado.Compras == true)
                {
                    CMSFornecedor.Enabled = true;
                }
                if (UserSession.UsuarioLogado.Empregado == true)
                {
                    CMSProduto.Enabled = true;

                }
            }
        }
        private void pictureBox11_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

       

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void CMSFolhaPagamento_Opening(object sender, CancelEventArgs e)
        {

        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Reino da pizza",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label10_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Reino da pizza",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void ptbProduto_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Reino da pizza",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void CMSProduto_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
           Folha_de_Pagamento tela = new Folha_de_Pagamento();
            tela.Show();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Telas.ConsultarFolhaDePagamento tela= new Telas.ConsultarFolhaDePagamento();
            tela.Show();
        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Essa Parte ainda não é acessível.", "Reino da pizza",
                       MessageBoxButtons.OK,
                       MessageBoxIcon.Error);
            /*this.Hide();
            CadastroCliente tela = new CadastroCliente();
            tela.Show();*/
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Essa Parte ainda não é acessível.", "Reino da pizza",
                       MessageBoxButtons.OK,
                       MessageBoxIcon.Error);
            /*this.Hide();
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();*/
        }

        private void novoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Essa Parte ainda não é acessível.", "Reino da pizza",
                       MessageBoxButtons.OK,
                       MessageBoxIcon.Error);
            /*this.Hide();
            CadastrarProduto tela = new CadastrarProduto();
            tela.Show();*/
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Essa Parte ainda não é acessível.", "Reino da pizza",
                       MessageBoxButtons.OK,
                       MessageBoxIcon.Error);
            /*this.Hide();
            Estoque tela = new Estoque();
            tela.Show();*/
        }

        private void novoToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Essa Parte ainda não é acessível.", "Reino da pizza",
                       MessageBoxButtons.OK,
                       MessageBoxIcon.Error);
            /* this.Hide();
             CadastrarFonecedor tela = new CadastrarFonecedor();
             tela.Show();*/
        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Essa Parte ainda não é acessível.", "Reino da pizza",
                       MessageBoxButtons.OK,
                       MessageBoxIcon.Error);
            /*this.Hide();
            ConsultarFornecedor tela = new ConsultarFornecedor();
            tela.Show();*/
        }

        private void novoToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Cadastro_De_Funcionário tela = new Cadastro_De_Funcionário();
            tela.Show();
        }

        private void consultarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConsultarFornecedor tela = new ConsultarFornecedor();
            tela.Show();
        }

        private void novoToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox11_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Reino da pizza",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void ptbCliente_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Reino da pizza",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void ptbFolhaPagamento_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Reino da pizza",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void ptbFornecedor_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Reino da pizza",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Reino da pizza",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Reino da pizza",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Reino da pizza",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Reino da pizza",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Reino da pizza",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {

            DialogResult r = MessageBox.Show("Realmente deseja deslogar do sistema ?", "Reino da pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                this.Hide();
                Login tela = new Login();
                tela.Show();
            }

        }

        private void toolStripMenuItem15_Click(object sender, EventArgs e)
        {

            DialogResult r = MessageBox.Show("Realmente deseja deslogar do sistema ?", "Reino da pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                this.Hide();
                Login tela = new Login();
                tela.Show();
            }
        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            this.Hide();
            Telas.ConsultarFolhaDePagamento tela = new Telas.ConsultarFolhaDePagamento();
            tela.Show();
        }

        private void label7_Click_1(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
           
        }

        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            this.Hide();
            Cadastro_De_Funcionário tela = new Cadastro_De_Funcionário();
            tela.Show();
        }

        private void toolStripMenuItem14_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConsultarFornecedor tela = new ConsultarFornecedor();
            tela.Show();
        }

       

        private void CMSFolhaPagamento_Opening_1(object sender, CancelEventArgs e)
        {

        }

        private void produtoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            CadastrarProduto tela = new CadastrarProduto();
            tela.Show();
        }

        private void produçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            CadastrarProduçao tela = new CadastrarProduçao();
            tela.Show();
        }

        private void pToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConsultarProduto tela = new ConsultarProduto();
            tela.Show();
        }

        private void CMSFornecedor_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            this.Hide();
            CadastrarFornecedor tela = new CadastrarFornecedor();
            tela.Show();
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Consultar_Fornecedor tela = new Consultar_Fornecedor();
            tela.Show();
        }

        private void CMSFuncionario_Opening(object sender, CancelEventArgs e)
        {

        }

       

        private void vendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Vendas tela = new Vendas();
            tela.Show();
        }

        private void compraToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConsultarCompra tela = new ConsultarCompra();
            tela.Show();
        }

        private void vendaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConsultarVenda tela = new ConsultarVenda();
            tela.Show();
        }

        private void CMSCliente_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            this.Hide();
            CadastroCliente tela = new CadastroCliente();
            tela.Show();
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void fluxoDeCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void produçãoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConsultarProducao tela = new ConsultarProducao();
            tela.Show();
        }

        private void folhaDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Folha_de_Pagamento tela = new Folha_de_Pagamento();
            tela.Show();
        }

        private void compraToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Compra tela = new Compra();
            tela.Show();
        }

        private void vendaToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Vendas tela = new Vendas();
            tela.Show();
        }

        private void gastosToolStripMenuItem_Click(object sender, EventArgs e)
        {

            this.Hide();
            G tela = new Vendas();
            tela.Show();
        }
    }
}
