﻿using Reino_da_pizza.DB.Fornecedor;
using Reino_da_pizza.DB.Produto;
using Reino_da_pizza.Telas.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class Consultar_Fornecedor : Form
    {
        public Consultar_Fornecedor()
        {
            InitializeComponent();
        }

        private void Consultar_Fornecedor_Load(object sender, EventArgs e)
        {

        }

        public void CarregarGrid()
        {
            string nome = txtnome.Text;
            FornecedorBusiness Business = new FornecedorBusiness();
            List<FornecedorDTO> lista = Business.Consultar();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }


        

        private void button1_Click_1(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 1)
                {
                    FornecedorDTO fornecedor = dataGridView1.CurrentRow.DataBoundItem as FornecedorDTO;

                    DialogResult r = MessageBox.Show("Deseja excluir o fornecedor? Ao excluir o fornecedor, todos seus produtos e pedidos serão excluidos automaticamente.", "Reino da Pizza",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        FornecedorBusiness business = new FornecedorBusiness();
                        ProdutoBusiness a = new ProdutoBusiness();
                        ItensPedidoBusiness b = new ItensPedidoBusiness();

                        b.Remover(fornecedor.Id);
                        a.Remover(fornecedor.Id);
                        business.Remover(fornecedor.Id);

                        CarregarGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                EnviarMensagemErro(ex.Message);
            }
            if (e.ColumnIndex == 0)
            {
                FornecedorDTO a = dataGridView1.CurrentRow.DataBoundItem as FornecedorDTO;

                DialogResult r = MessageBox.Show("Deseja alterar o fornecedor?", "Reino da Pizza",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)

                {
                    FornecedorDTO b = this.dataGridView1.Rows[e.RowIndex].DataBoundItem as FornecedorDTO;
                    this.Close();
                    AlterarFornecedor tela = new AlterarFornecedor();
                   tela.LoadScrean(b);
                    tela.ShowDialog();
                }


            }
        }


        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Information);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Consultar_Fornecedor_Load_1(object sender, EventArgs e)
        {

        }
    }
}
