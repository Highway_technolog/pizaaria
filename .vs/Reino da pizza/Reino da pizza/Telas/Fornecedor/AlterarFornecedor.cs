﻿using Newtonsoft.Json;
using Reino_da_pizza.DB.Cliente;
using Reino_da_pizza.DB.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas.Fornecedor
{
    public partial class AlterarFornecedor : Form
    {
        public AlterarFornecedor()
        {
            InitializeComponent();
            CarregarCombo();
        }
        public void CarregarCombo()
        {
            List<string> estado = new List<string>();
            estado.Add("Selecione um estado");
            estado.Add("Acre(AC)");
            estado.Add("Alagoas (AL)");
            estado.Add("Amapá (AP)");
            estado.Add("Amazonas (AM)");
            estado.Add("Bahia (BA)");
            estado.Add("Ceará (CE)");
            estado.Add("Distrito Federal (DF)");
            estado.Add("Espírito Santo (ES)");
            estado.Add("Goiás (GO)");
            estado.Add("Maranhão (MA)");
            estado.Add("Mato Grosso (MT)");
            estado.Add("Mato Grosso do Sul (MS)");
            estado.Add("Minas Gerais (MG)");
            estado.Add("Pará (PA) ");
            estado.Add("Paraíba (PB)");
            estado.Add("Paraná (PR)");
            estado.Add("Pernambuco (PE)");
            estado.Add("Piauí (PI)");
            estado.Add("Rio de Janeiro (RJ)");
            estado.Add("Rio Grande do Norte (RN)");
            estado.Add("Rio Grande do Sul (RS)");
            estado.Add("Rondônia (RO)");
            estado.Add("Roraima (RR)");
            estado.Add("Santa Catarina (SC)");
            estado.Add("São Paulo (SP)");
            estado.Add("Sergipe (SE)");
            estado.Add("Tocantins (TO)");

            cboEstado.DataSource = estado;
            cboEstado.SelectedItem = "Selecione um estado";

        }

        FornecedorDTO fornecedor;

        public void LoadScrean(FornecedorDTO fornecedor)
        {
            this.fornecedor = fornecedor;

            txtNome.Text = fornecedor.Nome;
            txtCNPJ.Text = fornecedor.CNPJ;
            txtContato.Text = fornecedor.Telefone;
            txtEmail.Text = fornecedor.Email;
            txtCep.Text = fornecedor.CEP;
            txtEndereco.Text = fornecedor.Endereco;
            cboEstado.SelectedItem = fornecedor.Cidade;
            txtNumero.Text = fornecedor.Numero;
            txtObs.Text = fornecedor.Observacao;

        }
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {

                this.fornecedor.Nome = txtNome.Text;
                this.fornecedor.CNPJ = txtCNPJ.Text;
                this.fornecedor.Telefone = txtContato.Text;
                this.fornecedor.Email = txtEmail.Text;
                this.fornecedor.CEP = txtCep.Text;
                this.fornecedor.Endereco = txtEndereco.Text;
                this.fornecedor.Cidade = cboEstado.SelectedItem.ToString();
                this.fornecedor.Numero = Convert.ToString(txtNumero.Text);
                this.fornecedor.Observacao = txtObs.Text;


                FornecedorBusiness business = new FornecedorBusiness();
                business.Alterar(fornecedor);
                EnviarMensagem("Fornecedor alterado com sucesso");

                this.Close();
                ConsultarFornecedor tela = new ConsultarFornecedor();
                tela.Show();
                tela.CarregarGrid();
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possível alterar o Fornecedor: " + ex.Message);
            }
        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
           
        }

        private void BuscarCep_Click(object sender, EventArgs e)
        {

            string cep = txtCep.Text.Trim().Replace("-", "");

            CorreioCliente correio = BuscarAPICorreio(cep);

            txtEndereco.Text = correio.Logradouro;
        }

        private CorreioCliente BuscarAPICorreio(string cep)
        {

            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;


            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");


            CorreioCliente correio = JsonConvert.DeserializeObject<CorreioCliente>(resposta);
            return correio;
        }
    }
    }

