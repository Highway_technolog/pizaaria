﻿using Newtonsoft.Json;
using Reino_da_pizza.DB.Cliente;
using Reino_da_pizza.DB.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class CadastrarFornecedor : Form
    {
        public CadastrarFornecedor()
        {
            InitializeComponent();
            CarregarCombo();
        }
        public void CarregarCombo()
        {
            List<string> estado = new List<string>();
            estado.Add("Selecione um estado");
            estado.Add("Acre(AC)");
            estado.Add("Alagoas (AL)");
            estado.Add("Amapá (AP)");
            estado.Add("Amazonas (AM)");
            estado.Add("Bahia (BA)");
            estado.Add("Ceará (CE)");
            estado.Add("Distrito Federal (DF)");
            estado.Add("Espírito Santo (ES)");
            estado.Add("Goiás (GO)");
            estado.Add("Maranhão (MA)");
            estado.Add("Mato Grosso (MT)");
            estado.Add("Mato Grosso do Sul (MS)");
            estado.Add("Minas Gerais (MG)");
            estado.Add("Pará (PA) ");
            estado.Add("Paraíba (PB)");
            estado.Add("Paraná (PR)");
            estado.Add("Pernambuco (PE)");
            estado.Add("Piauí (PI)");
            estado.Add("Rio de Janeiro (RJ)");
            estado.Add("Rio Grande do Norte (RN)");
            estado.Add("Rio Grande do Sul (RS)");
            estado.Add("Rondônia (RO)");
            estado.Add("Roraima (RR)");
            estado.Add("Santa Catarina (SC)");
            estado.Add("São Paulo (SP)");
            estado.Add("Sergipe (SE)");
            estado.Add("Tocantins (TO)");

            cboEstado.DataSource = estado;
            cboEstado.SelectedItem = "Selecione um estado";
        }


            private void button5_Click(object sender, EventArgs e)
            {
                try
                {
                    FornecedorDTO fornecedor = new FornecedorDTO();

                    fornecedor.Nome = txtNome.Text;
                    fornecedor.CNPJ = txtCNPJ.Text;
                    fornecedor.Telefone = txtContato.Text;
                    fornecedor.Email = txtEmail.Text;
                    fornecedor.CEP = txtCep.Text;
                    fornecedor.Endereco = txtEndereco.Text;
                    fornecedor.Cidade = cboEstado.SelectedItem.ToString();
                    fornecedor.Numero = Convert.ToString(txtNumero.Text);
                    fornecedor.Observacao = txtObs.Text;
                    fornecedor.Cadastro = DateTime.Now.Date;

                    FornecedorBusiness business = new FornecedorBusiness();
                    business.Salvar(fornecedor);




                    EnviarMensagem("Fornecedor salvo com sucesso");
                    EnviarMensagemNovo("Deseja cadastrar outro fornecedor ?");


                }
                catch (Exception ex)
                {
                    EnviarMensagemErro("Não foi possível cadastrar o fornecerdor: " + ex.Message);
                }
            }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Information);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }
        public void Limpar()
        {
            txtNome.Text = "";
            txtCNPJ.Text = "";
            txtContato.Text = "";
            txtEmail.Text = "";
            txtCep.Text = "";
            txtEndereco.Text = "";
            txtNumero.Text = "";
            cboEstado.SelectedItem = "Selecione um estado";
            txtObs.Text = "";


        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
            this.Close();
        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private void cboEstado_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void BuscarCep_Click(object sender, EventArgs e)
        {

            string cep = txtCep.Text.Trim().Replace("-", "");

            CorreioCliente correio = BuscarAPICorreio(cep);

            txtEndereco.Text = correio.Logradouro;
        }
        private CorreioCliente BuscarAPICorreio(string cep)
        {

            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;


            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");


            CorreioCliente correio = JsonConvert.DeserializeObject<CorreioCliente>(resposta);
            return correio;
        }
    }
}