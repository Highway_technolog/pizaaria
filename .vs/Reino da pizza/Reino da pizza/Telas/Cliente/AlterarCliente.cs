﻿using Newtonsoft.Json;
using Reino_da_pizza.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas.Cliente
{
    public partial class AlterarCliente : Form
    {
        public AlterarCliente()
        {
            InitializeComponent();
        }
        ClienteDTO dto;
        public void LoadScrean(ClienteDTO dto)
        {
            this.dto = dto;

            txtNome.Text = dto.Nome;
            txtRG.Text = dto.RG;
            txtCPF.Text = dto.CPF;
            txtCelular.Text = dto.Celular;
            txtEmail.Text = dto.Email;
            dtpNascimento.Value = dto.Nascimento;
            txtCep.Text = dto.CEP;
            txtEndereco.Text = dto.Endereço;
            txtNumero.Text = dto.Numero;
            txtCompleto.Text = dto.Complemento;
            txtTelefone.Text = dto.Telefone;
            txtObservacao.Text = dto.Observacao;

        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {

                this.dto.Nome = txtNome.Text;
                this.dto.RG = txtRG.Text;
                this.dto.CPF = txtCPF.Text;
                this.dto.Celular = txtCelular.Text;
                this.dto.Email = txtEmail.Text;
                this.dto.Nascimento = dtpNascimento.Value.Date;
                this.dto.CEP = txtCep.Text;
                this.dto.Endereço = txtEndereco.Text;
                this.dto.Numero = txtNumero.Text;
                this.dto.Complemento = txtCompleto.Text;
                this.dto.Telefone = txtTelefone.Text;
                this.dto.Observacao = txtObservacao.Text;


                ClienteBusiness buss = new ClienteBusiness();
                buss.Alterar(dto);

                EnviarMensagem("Cliente alterado com sucesso.");
                this.Close();
                ConsultarCliente tela = new ConsultarCliente();
                tela.Show();
                tela.CarregarGrid();
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Houve um erro ao alterar o cliente: " + ex.Message);
            }
        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.Close();
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
            tela.CarregarGrid();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void BuscarCep_Click(object sender, EventArgs e)
        {

            string cep = txtCep.Text.Trim().Replace("-", "");

            CorreioCliente correio = BuscarAPICorreio(cep);

            txtEndereco.Text = correio.Logradouro;
        }

        private CorreioCliente BuscarAPICorreio(string cep)
        {

            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;


            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");


            CorreioCliente correio = JsonConvert.DeserializeObject<CorreioCliente>(resposta);
            return correio;
        }
    }
    }

