﻿using Newtonsoft.Json;
using Reino_da_pizza.DB.Cliente;
using System;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class CadastroCliente : Form
    {
        public CadastroCliente()
        {
            InitializeComponent();
        }

        public void Limpar()
        {
            txtNome.Text = "";
            txtCPF.Text = "";
            txtCelular.Text = "";
            txtCep.Text = "";
            txtComplemento.Text = "";
            txtEmail.Text = "";
            txtEndereco.Text = "";
            txtNumero.Text = "";
            txtTelefone.Text = "";
        }
        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }
        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void CadastroCliente_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Nome = txtNome.Text;
                dto.RG = txtRG.Text;
                dto.CPF = txtCPF.Text;
                dto.Celular = txtCelular.Text;
                dto.Email = txtEmail.Text;
                dto.Nascimento = dtpNascimento.Value.Date;
                dto.CEP = txtCep.Text;
                dto.Endereço = txtEndereco.Text;
                dto.Numero = txtNumero.Text;
                dto.Complemento = txtComplemento.Text;
                dto.Telefone = txtTelefone.Text;
                dto.Observacao = txtObservacao.Text;

                ClienteBusiness buss = new ClienteBusiness();
                buss.Salvar(dto);



                EnviarMensagem("Cliente cadastrado com sucesso");
                EnviarMensagemNovo("Deseja cadastrar um novo cliente ?");
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possível cadastrar o cliente: " + ex.Message);
            }
        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void BuscarCep_Click(object sender, EventArgs e)
        {
             string cep = txtCep.Text.Trim().Replace("-", "");

                    CorreioCliente correio = BuscarAPICorreio(cep);

                    txtEndereco.Text = correio.Logradouro;
        }

        private CorreioCliente BuscarAPICorreio(string cep)
        {

            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;


            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");


            CorreioCliente correio = JsonConvert.DeserializeObject<CorreioCliente>(resposta);
            return correio;
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }
    }
}
