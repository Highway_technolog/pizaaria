﻿using Reino_da_pizza.DB.Estoque;
using Reino_da_pizza.DB.Fornecedor;
using Reino_da_pizza.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas.Compra
{
    public partial class Compra : Form
    {
        BindingList<ProduçaoDTO> produtosCarrinho = new BindingList<ProduçaoDTO>();
        BindingList<FornecedorDTO> fornecedor = new BindingList<FornecedorDTO>();

        public Compra()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }
        void ConfigurarGrid()
        {
            dgvCarrinho.AutoGenerateColumns = false;
            dgvCarrinho.DataSource = produtosCarrinho;
        }

        public void LoadScrean(EstoqueDTO pedido)
        {

            cbofor.SelectedItem = pedido.Fornecedor;
            cboproduto.SelectedItem = pedido.Produto;

        }

        void CarregarCombos()
        {


            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> listar = business.Listar();

            cbofor.SelectedItem = 0;
            cbofor.ValueMember = nameof(FornecedorDTO.Id);
            cbofor.DisplayMember = nameof(FornecedorDTO.Nome);
            cbofor.DataSource = listar;

            ProdutoBusiness business2 = new ProdutoBusiness();
            List<ProduçaoDTO> listar2 = business2.Listar();
            cboproduto.ValueMember = nameof(ProduçaoDTO.Id);
            cboproduto.DisplayMember = nameof(ProduçaoDTO.Nome);



            cboproduto.DataSource = listar2;




        }
        private void pictureBox8_Click(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void cbofor_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedorDTO func = cbofor.SelectedItem as FornecedorDTO;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO fornecedor = cbofor.SelectedItem as FornecedorDTO;


                PedidoDTO pedido = new PedidoDTO();

                pedido.NomeFornecedor = fornecedor.Nome;
                pedido.Data = DateTime.Now.Date;
                pedido.Valor = Convert.ToDecimal(lbltotal.Text);
                pedido.IdFornecedor = fornecedor.Id;


                PedidoBusiness t = new PedidoBusiness();
                t.Salvar(pedido, produtosCarrinho.ToList());


                EstoqueBusiness bus = new EstoqueBusiness();

                /*EstoqueDTO b =bus.ConsultarNome();
                if (b != null)
                {
                    bus.AdicionarItem();
                }
                else
                {
                    bus.Salvar();
                }*/


                EnviarMensagem("Compra finalizada com sucesso.");
                EnviarMensagemNovo("Deseja efetuar outra compra ?");



            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Ocorreu um erro ao finalizar a compra: " + ex.Message);
            }
        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                // Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProduçaoDTO dto = cboproduto.SelectedItem as ProduçaoDTO;
                if (txtquan.Text == "")
                {
                    throw new Exception("Informe a quantidade do produto.");
                }

                int qtd = Convert.ToInt32(txtquan.Text);

                for (int i = 0; i < qtd; i++)
                {
                    int qtd2 = 1;
                    produtosCarrinho.Add(dto);
                    decimal total = Convert.ToDecimal(lbltotal.Text);
                    decimal valor = Convert.ToDecimal(dto.ValorUnitario * qtd2);
                    decimal valor2 = valor + total;
                    lbltotal.Text = valor2.ToString();
                }
            }
            catch (Exception ex)
            {
                EnviarMensagemErro(ex.Message);
            }
        }

        private void dgvCarrinho_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                dgvCarrinho.Rows.RemoveAt(dgvCarrinho.CurrentRow.Index);

                ProduçaoDTO dto = cboproduto.SelectedItem as ProduçaoDTO;
                decimal total = Convert.ToDecimal(lbltotal.Text);
                decimal valor = Convert.ToDecimal((total - dto.ValorUnitario));
                lbltotal.Text = valor.ToString();
            }
        }
    }
}
