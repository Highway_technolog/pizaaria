﻿using Reino_da_pizza.DB.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas.Compra
{
    public partial class ConsultarCompra : Form
    {
        public ConsultarCompra()
        {
            InitializeComponent();
            dateTimePicker1.Value = DateTime.Now.Date;
        }
        void ConfigurarGrid()
        {
            DateTime a = DateTime.Now;

            PedidoBusiness Business = new PedidoBusiness();
            List<PedidoDTO> lista = Business.Consultar(dtp1.Value, dateTimePicker1.Value);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }
       

        
        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ConfigurarGrid();
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Aconteceu um erro ao consultar a compra: " + ex.Message);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}