﻿using Reino_da_pizza.DB.Fluxo_de_Caixa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas.Demais
{
    public partial class Fluxo_de_caixa : Form
    {
        public Fluxo_de_caixa()
        {
            InitializeComponent();
        }
        void CarregarGrid()
        {
            FluxodeCaixaBusiness business = new FluxodeCaixaBusiness();
            List<FluxodeCaixaDTO> lista = business.Consultar(dtpinicial.Value, dtpfinal.Value);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;

        }
        private void Fluxo_de_caixa_Load(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox3_Click_1(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }
    }
}