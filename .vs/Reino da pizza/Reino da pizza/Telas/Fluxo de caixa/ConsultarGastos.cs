﻿using Reino_da_pizza.DB.Fluxo_de_Caixa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas.Fluxo_de_caixa
{
    public partial class ConsultarGastos : Form
    {
        public ConsultarGastos()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConfigurarGrid();
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        public void Limpar()
        {

        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }
        public void ConfigurarGrid()
        {
            DateTime inicio = dtpinicio.Value;
            DateTime fim = dtpfim.Value;
            GastosBusiness Business = new GastosBusiness();
            List<GastosDTO> lista = Business.Consultar(inicio, fim);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                GastosDTO funcionario = dataGridView1.CurrentRow.DataBoundItem as GastosDTO;

                DialogResult r = MessageBox.Show("Deseja excluir esse gasto?", "Reino da Pizza",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    GastosBusiness business = new GastosBusiness();
                    business.Remover(funcionario.Id);

                    ConfigurarGrid();
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal a = new MenuPrincipal();
            a.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label4_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }
    }
}
