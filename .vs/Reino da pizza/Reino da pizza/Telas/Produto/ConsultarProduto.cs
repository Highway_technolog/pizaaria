﻿using Reino_da_pizza.DB.Fornecedor;
using Reino_da_pizza.DB.Produto;
using Reino_da_pizza.Telas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class ConsultarProduto : Form
    {
        public ConsultarProduto()
        {
            InitializeComponent();
        }
        void CarregarGrid()
        {
            string nome = txtNome.Text;
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProduçaoDTO> lista = business.Consultar();

            dgvProduto.AutoGenerateColumns = false;
            dgvProduto.DataSource = lista;
        }
        private void ConsultarProduto_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Information);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void dgvProduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 1)
                {
                    ProduçaoDTO fornecedor = dgvProduto.CurrentRow.DataBoundItem as ProduçaoDTO;

                    DialogResult r = MessageBox.Show("Deseja excluir o produto? Ao excluir o produto será excluido automaticamente todos os pedidos que conter esse produto.", "Reino da Pizza",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {

                        ProdutoBusiness a = new ProdutoBusiness();
                        ItensPedidoBusiness b = new ItensPedidoBusiness();

                        b.Remover(fornecedor.Id);
                        a.Remover(fornecedor.Id);


                        CarregarGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                EnviarMensagemErro(ex.Message);
            }
            if (e.ColumnIndex == 0)
            {
                ProduçaoDTO a = dgvProduto.CurrentRow.DataBoundItem as ProduçaoDTO;

                DialogResult r = MessageBox.Show("Deseja alterar o produto?", "Reino da Pizza",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ProduçaoDTO b = this.dgvProduto.Rows[e.RowIndex].DataBoundItem as ProduçaoDTO;
                    this.Close();
                    AlterarProduto tela = new AlterarProduto();
                    tela.LoadScrean(b);
                    tela.ShowDialog();
                }
            }
        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
    
}
