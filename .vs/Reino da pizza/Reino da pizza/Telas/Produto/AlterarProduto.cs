﻿using Reino_da_pizza.DB.Fornecedor;
using Reino_da_pizza.DB.Produto;
using Reino_da_pizza.Telas.Demais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas.Produto
{
    public partial class AlterarProduto : Form
    {
        public AlterarProduto()
        {
            InitializeComponent();
            CarregarCombos();
        }

        ProduçaoDTO produto;
        void CarregarCombos()
        {


            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> listar = business.Listar();



            cboFor.ValueMember = nameof(FornecedorDTO.Id);
            cboFor.DisplayMember = nameof(FornecedorDTO.Nome);
            cboFor.DataSource = listar;
        }

        public void LoadScrean(ProduçaoDTO produto)
        {




            this.produto = produto;


            txtnome.Text = produto.Nome;
            cboFor.SelectedItem = produto.IdFornecedor;
            comboBox1.Text = produto.Tipo;
            txtvalor.Text = Convert.ToString(produto.ValorUnitario);
            
            
            // funcionario.Foto = ImagemPlugin.ConverterParaString(imgFuncionario.Image);
            
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                FornecedorDTO forne = cboFor.SelectedItem as FornecedorDTO;


                ProduçaoDTO produto = new ProduçaoDTO();
                this.produto.Nome = txtnome.Text;
                this.produto.FornecedorNome = forne.Nome;
                this.produto.IdFornecedor = forne.Id;
                this.produto.Tipo = comboBox1.Text;
                this.produto.ValorUnitario = Convert.ToDecimal(txtvalor.Text);
                this.produto.Foto = ImagemPlugin.ConverterParaString(pictureBox3.Image);

                ProdutoBusiness business = new ProdutoBusiness();
                business.Alterar(produto);

                EnviarMensagem("Produto alterado com sucesso");

                ConsultarProduto a = new ConsultarProduto();
                a.Show();



            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possível alterar o produto: " + ex.Message);
            }
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }
    }
    }



