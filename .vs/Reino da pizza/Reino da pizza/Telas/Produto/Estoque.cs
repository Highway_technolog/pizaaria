﻿using Reino_da_pizza.DB.Estoque;
using Reino_da_pizza.Telas.Compra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class Estoque : Form
    {
        public Estoque()
        {
            InitializeComponent();
            ConfigurarGrid();
        }

        public void ConfigurarGrid()
        {
            EstoqueBusiness Business = new EstoqueBusiness();
            List<EstoqueDTO> lista = Business.Consultar();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void Estoque_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
       

        private void label3_Click(object sender, EventArgs e)
        {

        }

       


        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Information);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }



        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                DialogResult r = MessageBox.Show("Deseja comprar mais ?", "Panetteria Fiorentino",
                                     MessageBoxButtons.YesNo,
                                     MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    EstoqueDTO b = this.dataGridView1.Rows[e.RowIndex].DataBoundItem as EstoqueDTO;
                    this.Hide();
                    Compra a = new Compra();
                    a.LoadScrean(b);
                    a.ShowDialog();
                }
            }
            if (e.ColumnIndex == 5)
            {
                DialogResult r = MessageBox.Show("Deseja retirar o produto?", "Panetteria Fiorentino",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    EstoqueDTO dto = dataGridView1.Rows[e.RowIndex].DataBoundItem as EstoqueDTO;
                    if (dto.Quantidade == 0)
                    {
                        MessageBox.Show("Esse produto esgotou.", "Panetteria Fiorentino",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        EstoqueBusiness bus = new EstoqueBusiness();
                        bus.RemoverItem(dto);
                        ConfigurarGrid();
                    }
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text == "")
                {
                    ConfigurarGrid();
                }
                else
                {
                    string nome = textBox1.Text;
                    EstoqueBusiness Business = new EstoqueBusiness();
                    List<EstoqueDTO> lista = Business.ConsultarPorNome(nome);

                    dataGridView1.AutoGenerateColumns = false;
                    dataGridView1.DataSource = lista;
                }
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possivel realizar a consulta: " + ex.Message);
            }

        }
    }

}
