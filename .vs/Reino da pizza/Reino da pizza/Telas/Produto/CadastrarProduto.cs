﻿using Reino_da_pizza.DB.Fornecedor;
using Reino_da_pizza.DB.Produto;
using Reino_da_pizza.Telas.Demais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class CadastrarProduto : Form
    {
        public CadastrarProduto()
        {
            InitializeComponent();
            CarregarCombos();

        }

        void CarregarCombos()
        {


            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> listar = business.Listar();



            cboFornecedor.ValueMember = nameof(FornecedorDTO.Id);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Nome);
            cboFornecedor.DataSource = listar;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                FornecedorDTO forne = cboFornecedor.SelectedItem as FornecedorDTO;


                ProduçaoDTO produto = new ProduçaoDTO();
                produto.Nome = txtNome.Text;
                produto.FornecedorNome = forne.Nome;
                produto.IdFornecedor = forne.Id;
                produto.Tipo = comboBox1.Text;
                produto.ValorUnitario = Convert.ToDecimal(txtValor.Text);
                produto.Foto = ImagemPlugin.ConverterParaString(pictureBox3.Image);

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(produto);

                EnviarMensagem("Produto salvo com sucesso");
                EnviarMensagemNovo("Deseja cadastrar um novo produto ?");

            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possível cadastrar o produto: " + ex.Message);
            }
        }


        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino Da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino Da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino Da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Information);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        public void Limpar()
        {
            txtNome.Text = "";
            comboBox1.Text = "";
            txtValor.Text = "";
            cboFornecedor.SelectedText = "";
            pictureBox3.BackColor = Color.Gray;
        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Reino Da Pizza",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }

        private void cboFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedorDTO func = cboFornecedor.SelectedItem as FornecedorDTO;
        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label17_Click(object sender, EventArgs e)
        {

            EnviarMensagemFechar();
        }

        private void PicProduto_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                pictureBox3.ImageLocation = dialog.FileName;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }
    }
    
}
