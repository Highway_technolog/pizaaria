﻿using Reino_da_pizza.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas.Venda.Produção
{
    public partial class CadastrarProduçao : Form
    {
        public CadastrarProduçao()
        {
            InitializeComponent();
            CarregarCombo();

        }
        public void CarregarCombo()
        {
            List<string> tipo = new List<string>();
            tipo.Add("Selecione");
            tipo.Add("Revenda");
            tipo.Add("Produção");

            cboTipo.DataSource = tipo;
            cboTipo.SelectedText = "Selecione";
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                txtNome.Text = "";
                txtValor.Text = "";
                txtDes.Text = "";
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }
        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProducaoDTO dto = new ProducaoDTO();
                dto.Nome = txtNome.Text;
                dto.Valor = Convert.ToDecimal(txtValor.Text);
                dto.Descricao = txtDes.Text;

                ProducaoBusiness business = new ProducaoBusiness();
                business.Salvar(dto);


                EnviarMensagem("Item do cardapio salvo com sucesso");
                EnviarMensagemNovo("Deseja cadastrar outro item ?");
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Houve um erro ao salvar o item: " + ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtNome.Text = "";
            txtValor.Text = "";
            txtDes.Text = "";
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
