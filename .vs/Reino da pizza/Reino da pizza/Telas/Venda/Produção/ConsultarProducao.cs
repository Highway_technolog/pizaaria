﻿using Reino_da_pizza.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas.Venda.Produção
{
    public partial class ConsultarProducao : Form
    {
        public ConsultarProducao()
        {
            InitializeComponent();
        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        public void CarregarGrid()
        {

            ProducaoBusiness Business = new ProducaoBusiness();
            List<ProducaoDTO> lista = Business.Consultar();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }
        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 1)
                {
                    ProducaoDTO producao = dataGridView1.CurrentRow.DataBoundItem as ProducaoDTO;

                    DialogResult r = MessageBox.Show("Deseja excluir o item? Ao excluir o fornecedor, todos seus produtos e pedidos serão excluidos automaticamente.", "Reino da Pizza",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        ProducaoBusiness business = new ProducaoBusiness();
                        /*ProdutoBusiness a = new ProdutoBusiness();
                        ItensPedidoBusiness b = new ItensPedidoBusiness();

                        b.Remover(fornecedor.Id);
                        a.Remover(fornecedor.Id);*/
                        business.Remover(producao.Id);

                        CarregarGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                EnviarMensagemErro(ex.Message);
            }
            if (e.ColumnIndex == 0)
            {
                ProducaoDTO a = dataGridView1.CurrentRow.DataBoundItem as ProducaoDTO;

                DialogResult r = MessageBox.Show("Deseja alterar o item?", "Reino da Pizza",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ProducaoDTO b = this.dataGridView1.Rows[e.RowIndex].DataBoundItem as ProducaoDTO;
                    this.Close();
                    AlterarProducao tela = new AlterarProducao();
                    tela.LoadScrean(b);
                    tela.ShowDialog();
                }
            }
        }

    }

}
