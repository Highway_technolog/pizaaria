﻿using Reino_da_pizza.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas.Venda.Produção
{
    public partial class AlterarProducao : Form
    {
        public AlterarProducao()
        {
            InitializeComponent();
            CarregarCombo();
        }

        ProducaoDTO produtos;
        public void CarregarCombo()
        {
            List<string> tipo = new List<string>();
            tipo.Add("Selecione");
            tipo.Add("Revenda");
            tipo.Add("Produção");

            cboTipo.DataSource = tipo;
            cboTipo.SelectedItem = "Selecione";
        }
        public void LoadScrean(ProducaoDTO produto)
        {
            this.produtos = produto;


            txtNome.Text = produto.Nome;
            txtValor.Text = Convert.ToString(produto.Valor);
            txtDes.Text = produto.Descricao;


        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void pictureBox9_Click(object sender, EventArgs e)
        {
            ConsultarProducao tela = new ConsultarProducao();
            this.Close();
            tela.Show();
            tela.CarregarGrid();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.produtos.Nome = txtNome.Text;
                this.produtos.Valor = Convert.ToDecimal(txtValor.Text);
                this.produtos.Descricao = txtDes.Text;

                ProducaoBusiness business = new ProducaoBusiness();
                business.Alterar(produtos);


                EnviarMensagem("Item do cardapio alterado com sucesso");
                ConsultarProducao tela = new ConsultarProducao();
                this.Close();
                tela.Show();
                tela.CarregarGrid();
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Houve erro ao alterar o item: " + ex.Message);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }
    }
}
