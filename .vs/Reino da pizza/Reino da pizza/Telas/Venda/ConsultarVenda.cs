﻿using Reino_da_pizza.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas
{
    public partial class ConsultarVenda : Form
    {
        public ConsultarVenda()
        {
            InitializeComponent();
            dateTimePicker1.Value = DateTime.Now.Date;
        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        void CarregarGrid()
        {
            DateTime data = dtp1.Value;
            EncomendaBusiness business = new EncomendaBusiness();
            List<EncomendaDTO> lista = business.Consultar(dtp1.Value, dateTimePicker1.Value);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }
        
        private void pictureBox8_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void label5_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ConsultarVenda_Load(object sender, EventArgs e)
        {

        }
    }
}
