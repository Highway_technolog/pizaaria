﻿using Reino_da_pizza.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas.Venda
{
    public partial class PedidosPendentes : Form
    {
        public PedidosPendentes()
        {
            InitializeComponent();
            CarregarGrid();
        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }

        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }
        public void CarregarGrid()
        {

            EncomendaBusiness Business = new EncomendaBusiness();
            List<EncomendaDTO> lista = Business.ConsultarStatus();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5)
                {
                    EncomendaDTO producao = dataGridView1.CurrentRow.DataBoundItem as EncomendaDTO;

                    DialogResult r = MessageBox.Show("Deseja Finalizar o Pedido ?", "Reino da Pizza",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        EncomendaBusiness b = new EncomendaBusiness();
                        b.Alterar(producao.ID);


                        CarregarGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                EnviarMensagemErro(ex.Message);
            }
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
