﻿using Newtonsoft.Json;
using Panetteria_Fiorentina.DB.PLUGIN;
using Reino_da_pizza.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas.Venda
{
    public partial class Vendas : Form
    {

        BindingList<ProducaoDTO> produtosCarrinho = new BindingList<ProducaoDTO>();
        BindingList<ClienteDTO> cliente = new BindingList<ClienteDTO>();
        public Vendas()
        {
            InitializeComponent();
            cbo1.Visible = false;
            CarregarCombos();
            ConfigurarGrid();
            cboPagamento.SelectedText = "Selecione";
            cbo1.Visible = false;
            lblemail.Visible = false;
            txtEmail.Visible = false;
            txtTelefone.Visible = false;
            txtCep.Visible = false;
            txtEndereco.Visible = false;
            txtnum.Visible = false;
            txtcomplemento.Visible = false;
            lblc.Visible = false;
            lblce.Visible = false;
            lblco.Visible = false;
            lble.Visible = false;
            lblf.Visible = false;
            lbln.Visible = false;
            lblt.Visible = false;
            cboPagamento.Visible = false;
            cbo1.Visible = false;
            txtnome.Visible = false;
            btnBuscarCEP.Visible = false;
            label2.Visible = false;
            cboRetirada.Visible = false;

            cboPagamento.SelectedItem = "Selecione";
            cboRetirada.SelectedItem = "Selecione";


        }
        public void Limpar()
        {
            for (int i = 0; i < dgvCarrinho.RowCount; i++)
            {
                dgvCarrinho.Rows[i].DataGridView.Rows.Clear();
            }
            cbo1.Visible = false;
            CarregarCombos();
            ConfigurarGrid();
            cboPagamento.SelectedText = "Selecione";
            cbo1.Visible = false;
            lblemail.Visible = false;
            txtEmail.Visible = false;
            txtTelefone.Visible = false;
            txtCep.Visible = false;
            txtEndereco.Visible = false;
            txtnum.Visible = false;
            txtcomplemento.Visible = false;
            lblc.Visible = false;
            lblce.Visible = false;
            lblco.Visible = false;
            lble.Visible = false;
            lblf.Visible = false;
            lbln.Visible = false;
            lblt.Visible = false;
            cboPagamento.Visible = false;
            cbo1.Visible = false;
            txtnome.Visible = false;
            btnBuscarCEP.Visible = false;
            label2.Visible = false;
            cboRetirada.Visible = false;

            radioButton1.Checked = false;
            radioButton2.Checked = false;

            txtquan.Text = "";
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> listar = business.Listar();
            cboPagamento.SelectedItem = "Selecione";
            cboRetirada.SelectedItem = "Selecione";
            txtCep.Text = "";
            txtTelefone.Text = "";
            txtEndereco.Text = "";
            txtcomplemento.Text = "";
            txtnum.Text = "";
            txtEmail.Text = "";
            txtnome.Text = "";
            cboPagamento.SelectedItem = 0;
            cboRetirada.SelectedItem = 0;
            txtTelefone.Text = "";
            txtCep.Text = "";
            txtEndereco.Text = "";
            txtcomplemento.Text = "";
            txtnum.Text = "";
            txtEmail.Text = "";
            txtnome.Text = "";

            cboRetirada.SelectedItem = 0;
            cbo1.ValueMember = nameof(ClienteDTO.ID);
            cbo1.DisplayMember = nameof(ClienteDTO.Nome);
            cbo1.DataSource = listar;
        }
        void CarregarCombos()
        {
            ProducaoBusiness business2 = new ProducaoBusiness();
            List<ProducaoDTO> listar2 = business2.Listar();
            cbo2.ValueMember = nameof(ProducaoDTO.Id);
            cbo2.DisplayMember = nameof(ProducaoDTO.Nome);



            cbo2.DataSource = listar2;
        }
        void ConfigurarGrid()
        {
            dgvCarrinho.AutoGenerateColumns = false;
            dgvCarrinho.DataSource = produtosCarrinho;
        }

        private void Vendas_Load(object sender, EventArgs e)
        {

        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }

        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProducaoDTO dto = cbo2.SelectedItem as ProducaoDTO;
                if (txtquan.Text == "")
                {
                    throw new Exception("Informe a quantidade do produto");
                }

                decimal qtd = Convert.ToInt32(txtquan.Text);

                for (int i = 0; i < qtd; i++)
                {
                    int qtd2 = 1;
                    produtosCarrinho.Add(dto);
                    decimal total = Convert.ToDecimal(lbltotal.Text);
                    decimal valor = Convert.ToDecimal(dto.Valor * qtd2);
                    decimal valor2 = valor + total;
                    lbltotal.Text = valor2.ToString();
                }
            }
            catch (Exception ex)
            {
                EnviarMensagemErro(ex.Message);
            }

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                cbo1.Visible = false;
                lblemail.Visible = false;
                txtEmail.Visible = false;
                btnBuscarCEP.Visible = true;
                txtTelefone.Visible = true;
                txtCep.Visible = true;
                txtEndereco.Visible = true;
                txtnum.Visible = true;
                txtcomplemento.Visible = true;
                lblc.Visible = true;
                lblce.Visible = true;
                lblco.Visible = true;
                lble.Visible = true;
                lblf.Visible = true;
                lbln.Visible = true;
                lblt.Visible = true;
                cboPagamento.Visible = true;
                txtnome.Visible = true;
                label2.Visible = true;
                cboRetirada.Visible = true;


                txtCep.Text = "";
                txtTelefone.Text = "";
                txtEndereco.Text = "";
                txtcomplemento.Text = "";
                txtnum.Text = "";
                txtEmail.Text = "";
                txtnome.Text = "";
                cboPagamento.SelectedItem = 0;
                cboRetirada.SelectedItem = 0;

            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked == true)
            {
                txtnome.Visible = false;
                btnBuscarCEP.Visible = false;
                cbo1.Visible = true;

                lblemail.Visible = true;
                txtEmail.Visible = true;
                txtTelefone.Visible = true;
                txtCep.Visible = true;
                txtEndereco.Visible = true;
                txtnum.Visible = true;
                txtcomplemento.Visible = true;
                lblc.Visible = true;
                lblce.Visible = true;
                lblco.Visible = true;
                lble.Visible = true;
                lblf.Visible = true;
                lbln.Visible = true;
                lblt.Visible = true;
                cboPagamento.Visible = true;
                label2.Visible = true;
                cboRetirada.Visible = true;


                ClienteBusiness business = new ClienteBusiness();
                List<ClienteDTO> listar = business.Listar();

                txtTelefone.Text = "";
                txtCep.Text = "";
                txtEndereco.Text = "";
                txtcomplemento.Text = "";
                txtnum.Text = "";
                txtEmail.Text = "";
                txtnome.Text = "";
                cbo1.SelectedItem = 0;
                cboPagamento.SelectedItem = 0;
                cboRetirada.SelectedItem = 0;
                cbo1.ValueMember = nameof(ClienteDTO.ID);
                cbo1.DisplayMember = nameof(ClienteDTO.Nome);
                cbo1.DataSource = listar;




            }
        }

        private void btnBuscarCEP_Click(object sender, EventArgs e)
        {
            string cep = txtCep.Text.Trim().Replace("-", "");

            CorreioCliente correio = BuscarAPICorreio(cep);

            txtEndereco.Text = correio.Logradouro;
        }
        private CorreioCliente BuscarAPICorreio(string cep)
        {

            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;


            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");


            CorreioCliente correio = JsonConvert.DeserializeObject<CorreioCliente>(resposta);
            return correio;
        }

        private void dgvCarrinho_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                dgvCarrinho.Rows.RemoveAt(dgvCarrinho.CurrentRow.Index);
                ProducaoDTO dto = cbo2.SelectedItem as ProducaoDTO;
                decimal total = Convert.ToDecimal(lbltotal.Text);
                decimal valor = Convert.ToDecimal((total - dto.Valor));
                lbltotal.Text = valor.ToString();

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (radioButton1.Checked == true)
                {
                    ClienteEventualDTO dto = new ClienteEventualDTO();
                    dto.Nome = txtnome.Text;
                    dto.Endereço = txtEndereco.Text;
                    dto.Telefone = txtTelefone.Text;
                    dto.Numero = txtnum.Text;
                    dto.CEP = txtCep.Text;
                    dto.Complemento = txtcomplemento.Text;

                    ClienteEventualBusiness bus = new ClienteEventualBusiness();
                    bus.Salvar(dto);

                    EncomendaDTO enc = new EncomendaDTO();
                    enc.Data = DateTime.Now.Date;
                    enc.IdClienteEventual = dto.ID;
                    enc.Valor = Convert.ToDecimal(lbltotal.Text);
                    enc.Cliente = dto.Nome;
                    enc.Pagamento = cboPagamento.SelectedItem.ToString();
                    enc.Entrega = cboRetirada.SelectedItem.ToString();

                    EncomendaBusiness b = new EncomendaBusiness();
                    b.Salvar(enc, produtosCarrinho.ToList());

                }
                if (radioButton2.Checked == true)
                {
                    ClienteDTO cliente = cbo1.SelectedItem as ClienteDTO;

                    EncomendaDTO enc = new EncomendaDTO();
                    enc.Data = DateTime.Now.Date;
                    enc.IdCliente = cliente.ID;
                    enc.Valor = Convert.ToDecimal(lbltotal.Text);
                    enc.Cliente = cliente.Nome;
                    enc.Pagamento = cboPagamento.SelectedItem.ToString();
                    enc.Entrega = cboRetirada.SelectedItem.ToString();


                    EncomendaBusiness b = new EncomendaBusiness();
                    b.Salvar(enc, produtosCarrinho.ToList());



                    if (cboRetirada.Text == "Delivery")
                    {
                        string nome = cbo1.Text;
                        string cep = txtCep.Text;
                        string rua = txtEndereco.Text;
                        string numero = txtnum.Text;
                        string valor = lbltotal.Text;
                        string conteudo = string.Empty;
                        string pagamento = cboPagamento.Text;
                        string retirada = cboRetirada.Text;



                        foreach (ProducaoDTO item in produtosCarrinho)
                        {
                            conteudo = conteudo + " - " + item.Nome + " - " + item.Valor + "\n";

                        }
                        conteudo = "Olá, " + nome + " seu pedido ja foi finalizado.\n Ele foi solicitado para o endereço: \n \n CEP -" + cep + "\n \n Rua -" + rua + " \n \n Numero -" + numero + "\n \n E dentre de 15 - 30 minutos estará pronto \n \n Foi solicitado: \n" + conteudo + " No valor de: " + valor + "\n Forma de pagamento: " + pagamento + "\n Forma de retirada : " + retirada;


                        Email email = new Email();
                        email.Enviar(txtEmail.Text, conteudo);
                    }
                    else
                    {
                        string nome = cbo1.Text;
                        string cep = txtCep.Text;
                        string rua = txtEndereco.Text;
                        string numero = txtnum.Text;
                        string valor = lbltotal.Text;
                        string conteudo = string.Empty;
                        string pagamento = cboPagamento.Text;
                        string retirada = cboRetirada.Text;
                        conteudo = "Olá, " + nome + " seu pedido ja foi finalizado. /n /n Aguarde um instante";

                        Email email = new Email();
                        email.Enviar(txtEmail.Text, conteudo);
                    }


                }

                EnviarMensagem("Venda efetuada com sucesso");
                EnviarMensagemNovo("Deseja efetuar uma nova venda ?");
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Houve um erro ao efutar a compra: " + ex.Message);

                if (ex.Message.Contains("em falta"))
                {
                    DialogResult r = MessageBox.Show("Deseja ir ao estoque ?", "Reino da Pizza",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        this.Hide();
                        Estoque a = new Estoque();
                        a.Show();
                    }
                }



            }
        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label18_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal a = new MenuPrincipal();
            a.Show();
        }
    }
    
}
