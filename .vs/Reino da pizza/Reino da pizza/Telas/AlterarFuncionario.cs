﻿using Reino_da_pizza.DB.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class AlterarFuncionario : Form
    {
        public AlterarFuncionario()
        {
            InitializeComponent();
        }


        FuncionarioDTO funcionario;

        public void LoadScrean(FuncionarioDTO funcionario)
        {
            this.funcionario = funcionario;


            NomeText.Text = funcionario.Nome;
            CPFtext.Text = funcionario.CPF;
            EndercoText.Text = funcionario.Endereço;
            Numerotext.Text = funcionario.Numero;
            CEPtext.Text = funcionario.CEP;
            Celtext.Text = funcionario.Celular;
            DTPNascimento.Value = funcionario.Nascimento;
            salarioText.Text = Convert.ToString(funcionario.Salario);
            Emailtext.Text = funcionario.Email;
            ComplementoText.Text = funcionario.Complemento;
            // funcionario.Foto = ImagemPlugin.ConverterParaString(imgFuncionario.Image);
            RGtext.Text = funcionario.RG;
            Teltext.Text = funcionario.Telefone;
            CBCArgo.Text = funcionario.Cargo;


            Usuariotext.Text = funcionario.Usuario;
            senhaText.Text = funcionario.Senha;
            chkAdmin.Checked = funcionario.Admin;
            chkRH.Checked = funcionario.RH;
            chkLogistica.Checked = funcionario.Logistica;
            chkFinanceiro.Checked = funcionario.Financeiro;
            chkVendas.Checked = funcionario.Vendas;
            chkCompras.Checked = funcionario.Compras;
            chkEmpregado.Checked = funcionario.Empregado;

        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }

        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }


        private void button5_Click(object sender, EventArgs e)
        {
            try
            {

                this.funcionario.Nome = NomeText.Text;
                this.funcionario.CPF = CPFtext.Text;
                this.funcionario.Endereço = EndercoText.Text;
                this.funcionario.Numero = Numerotext.Text;
                this.funcionario.CEP = CEPtext.Text;
                this.funcionario.Celular = Celtext.Text;
                this.funcionario.Nascimento = DTPNascimento.Value;
                this.funcionario.Salario = Convert.ToDecimal(salarioText.Text);
                this.funcionario.Email = Emailtext.Text;
                this.funcionario.Complemento = ComplementoText.Text;
                // funcionario.Foto = ImagemPlugin.ConverterParaString(imgFuncionario.Image);
                this.funcionario.RG = RGtext.Text;
                this.funcionario.Telefone = Teltext.Text;
                this.funcionario.Cargo = CBCArgo.Text;


                this.funcionario.Usuario = Usuariotext.Text;
                this.funcionario.Senha = senhaText.Text;
                this.funcionario.Admin = chkAdmin.Checked;
                this.funcionario.RH = chkRH.Checked;
                this.funcionario.Logistica = chkLogistica.Checked;
                this.funcionario.Financeiro = chkFinanceiro.Checked;
                this.funcionario.Vendas = chkVendas.Checked;
                this.funcionario.Compras = chkCompras.Checked;
                this.funcionario.Empregado = chkEmpregado.Checked;




                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Alterar(funcionario);

                EnviarMensagem("Funcionário alterado com sucesso.");


                this.Hide();

                ConsultarFuncionário cadastro = new ConsultarFuncionário();

                cadastro.Show();


            }

            catch (Exception ex)
            {
                EnviarMensagemErro("Ocorreu um erro ao alterar o funcionário: " + ex.Message);
            }
        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label18_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AlterarFuncionario tela = new AlterarFuncionario();
            tela.LoadScrean(funcionario);
        }
    }
}
