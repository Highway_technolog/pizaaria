﻿using Newtonsoft.Json;
using Reino_da_pizza.DB.Cliente;
using Reino_da_pizza.DB.Funcionário;
using Reino_da_pizza.Telas.Demais;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class Cadastro_De_Funcionário : Form
    {
        public Cadastro_De_Funcionário()
        {
            InitializeComponent();
            Cargos.Add(cargo0);
            
            Cargos.Add(cargo2);
            Cargos.Add(cargo3);
           
            Cargos.Add(cargo5);
            
            Cargos.Add(cargo7);
            Cargos.Add(cargo8);
           
            Cargos.Add(cargo10);
            Cargos.Add(cargo11);
            Cargos.Add(cargo12);
            Cargos.Add(cargo13);

            CBCArgo.DataSource = Cargos;
            CBCArgo.SelectedText = "Selecione o Cargo";



        }
        List<String> Cargos = new List<string>();
        string cargo0 = "Selecione o Cargo";
        
        string cargo2 = "Atendente";
        string cargo3 = "Balconista";
       
        string cargo5 = "Entregador";
       
        string cargo7 = "Auxiliar de caixa";
        string cargo8 = "Operador de Caixa";
        
        string cargo10 = "Pizzaiolo";
        string cargo11 = "Repositor";
        string cargo12 = "Administrador";
        string cargo13 = "Gerente";
        
                

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {


                FuncionarioDTO funcionario = new FuncionarioDTO();
                funcionario.Nome = NomeText.Text;
                funcionario.CPF = CPFtext.Text;
                funcionario.Endereço = EndercoText.Text;
                funcionario.Numero = Numerotext.Text;
                funcionario.CEP = txtcep.Text;
                funcionario.Celular = Celtext.Text;
                funcionario.Nascimento = DTPNascimento.Value.Date;
                funcionario.Salario = Convert.ToDecimal(salarioText.Text);
                funcionario.Email = Emailtext.Text;
                funcionario.Complemento = ComplementoText.Text;
                if (imgFuncionario.BackgroundImage != null)
                {
                    funcionario.Foto = ImagemPlugin.ConverterParaString(imgFuncionario.Image);
                }

                funcionario.RG = RGtext.Text;
                funcionario.Telefone = Teltext.Text;
                funcionario.Cargo = CBCArgo.Text;


                funcionario.Usuario = Usuariotext.Text;
                funcionario.Senha = senhaText.Text;
                funcionario.Admin = chkAdmin.Checked;
                funcionario.RH = chkRH.Checked;
                funcionario.Logistica = chkLogistica.Checked;
                funcionario.Financeiro = chkFinanceiro.Checked;
                funcionario.Vendas = chkVendas.Checked;
                funcionario.Compras = chkCompras.Checked;
                funcionario.Empregado = chkEmpregado.Checked;




                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(funcionario);

                EnviarMensagem("Funcionário salvo com sucesso.");
                EnviarMensagemNovo("Deseja cadastrar outro funcionário ?");






            }


            catch (Exception ex)
            {
                EnviarMensagemErro("Ocorreu um erro ao salvar o funcionário: " + ex.Message);
            }

        }
        public void Limpar()
        {
            NomeText.Text = "";
            CPFtext.Text = "";
            EndercoText.Text = "";
            Numerotext.Text = "";
            txtcep.Text = "";
            Celtext.Text = "";

            salarioText.Text = "";
            Emailtext.Text = "";
            ComplementoText.Text = "";
            RGtext.Text = "";
            Teltext.Text = "";
            CBCArgo.Text = "";


            Usuariotext.Text = "";
            senhaText.Text = "";
            chkAdmin.Checked = false;
            chkRH.Checked = false;
            chkLogistica.Checked = false;
            chkFinanceiro.Checked = false;
            chkVendas.Checked = false;
            chkCompras.Checked = false;
            chkEmpregado.Checked = false;
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }

        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }


        private void button4_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private void label18_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void CBCArgo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Cadastro_De_Funcionário_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        
        private void BuscarCep_Click(object sender, EventArgs e)
        {
            string cep = txtcep.Text.Trim().Replace("-", "");

            CorreioCliente correio = BuscarAPICorreio(cep);

            EndercoText.Text = correio.Logradouro;
        }
        private CorreioCliente BuscarAPICorreio(string cep)
        {

            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;


            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");


            CorreioCliente correio = JsonConvert.DeserializeObject<CorreioCliente>(resposta);
            return correio;
        }

        private void txCep_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void imgFuncionario_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgFuncionario.ImageLocation = dialog.FileName;
            }
        }
    }
}
