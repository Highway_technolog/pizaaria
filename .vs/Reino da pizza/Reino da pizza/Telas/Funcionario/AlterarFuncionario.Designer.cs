﻿namespace Reino_da_pizza
{
    partial class AlterarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlterarFuncionario));
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.chkCompras = new System.Windows.Forms.CheckBox();
            this.chkVendas = new System.Windows.Forms.CheckBox();
            this.chkFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkRH = new System.Windows.Forms.CheckBox();
            this.chkLogistica = new System.Windows.Forms.CheckBox();
            this.chkEmpregado = new System.Windows.Forms.CheckBox();
            this.chkAdmin = new System.Windows.Forms.CheckBox();
            this.salarioText = new System.Windows.Forms.TextBox();
            this.CBCArgo = new System.Windows.Forms.ComboBox();
            this.Emailtext = new System.Windows.Forms.MaskedTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.Numerotext = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.button5 = new System.Windows.Forms.Button();
            this.lblPermissão = new System.Windows.Forms.Label();
            this.senhaText = new System.Windows.Forms.MaskedTextBox();
            this.Usuariotext = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.DTPNascimento = new System.Windows.Forms.DateTimePicker();
            this.EndercoText = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.imgFuncionario = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.CEPtext = new System.Windows.Forms.MaskedTextBox();
            this.ComplementoText = new System.Windows.Forms.MaskedTextBox();
            this.RGtext = new System.Windows.Forms.MaskedTextBox();
            this.CPFtext = new System.Windows.Forms.MaskedTextBox();
            this.Celtext = new System.Windows.Forms.MaskedTextBox();
            this.Teltext = new System.Windows.Forms.MaskedTextBox();
            this.NomeText = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.BuscarCep = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(711, 467);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(100, 73);
            this.pictureBox5.TabIndex = 231;
            this.pictureBox5.TabStop = false;
            // 
            // chkCompras
            // 
            this.chkCompras.AutoSize = true;
            this.chkCompras.BackColor = System.Drawing.Color.Transparent;
            this.chkCompras.Font = new System.Drawing.Font("Copperplate Gothic Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCompras.ForeColor = System.Drawing.Color.Gold;
            this.chkCompras.Location = new System.Drawing.Point(562, 448);
            this.chkCompras.Name = "chkCompras";
            this.chkCompras.Size = new System.Drawing.Size(91, 19);
            this.chkCompras.TabIndex = 230;
            this.chkCompras.Text = "Compras";
            this.chkCompras.UseVisualStyleBackColor = false;
            // 
            // chkVendas
            // 
            this.chkVendas.AutoSize = true;
            this.chkVendas.BackColor = System.Drawing.Color.Transparent;
            this.chkVendas.Font = new System.Drawing.Font("Copperplate Gothic Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkVendas.ForeColor = System.Drawing.Color.Gold;
            this.chkVendas.Location = new System.Drawing.Point(562, 424);
            this.chkVendas.Name = "chkVendas";
            this.chkVendas.Size = new System.Drawing.Size(80, 19);
            this.chkVendas.TabIndex = 229;
            this.chkVendas.Text = "Vendas";
            this.chkVendas.UseVisualStyleBackColor = false;
            // 
            // chkFinanceiro
            // 
            this.chkFinanceiro.AutoSize = true;
            this.chkFinanceiro.BackColor = System.Drawing.Color.Transparent;
            this.chkFinanceiro.Font = new System.Drawing.Font("Copperplate Gothic Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFinanceiro.ForeColor = System.Drawing.Color.Gold;
            this.chkFinanceiro.Location = new System.Drawing.Point(562, 400);
            this.chkFinanceiro.Name = "chkFinanceiro";
            this.chkFinanceiro.Size = new System.Drawing.Size(107, 19);
            this.chkFinanceiro.TabIndex = 228;
            this.chkFinanceiro.Text = "Financeiro";
            this.chkFinanceiro.UseVisualStyleBackColor = false;
            // 
            // chkRH
            // 
            this.chkRH.AutoSize = true;
            this.chkRH.BackColor = System.Drawing.Color.Transparent;
            this.chkRH.Font = new System.Drawing.Font("Copperplate Gothic Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRH.ForeColor = System.Drawing.Color.Gold;
            this.chkRH.Location = new System.Drawing.Point(421, 470);
            this.chkRH.Name = "chkRH";
            this.chkRH.Size = new System.Drawing.Size(48, 19);
            this.chkRH.TabIndex = 227;
            this.chkRH.Text = "RH";
            this.chkRH.UseVisualStyleBackColor = false;
            // 
            // chkLogistica
            // 
            this.chkLogistica.AutoSize = true;
            this.chkLogistica.BackColor = System.Drawing.Color.Transparent;
            this.chkLogistica.Font = new System.Drawing.Font("Copperplate Gothic Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLogistica.ForeColor = System.Drawing.Color.Gold;
            this.chkLogistica.Location = new System.Drawing.Point(420, 449);
            this.chkLogistica.Name = "chkLogistica";
            this.chkLogistica.Size = new System.Drawing.Size(94, 19);
            this.chkLogistica.TabIndex = 226;
            this.chkLogistica.Text = "Logistica";
            this.chkLogistica.UseVisualStyleBackColor = false;
            // 
            // chkEmpregado
            // 
            this.chkEmpregado.AutoSize = true;
            this.chkEmpregado.BackColor = System.Drawing.Color.Transparent;
            this.chkEmpregado.Font = new System.Drawing.Font("Copperplate Gothic Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEmpregado.ForeColor = System.Drawing.Color.Gold;
            this.chkEmpregado.Location = new System.Drawing.Point(420, 424);
            this.chkEmpregado.Name = "chkEmpregado";
            this.chkEmpregado.Size = new System.Drawing.Size(108, 19);
            this.chkEmpregado.TabIndex = 225;
            this.chkEmpregado.Text = "Empregado";
            this.chkEmpregado.UseVisualStyleBackColor = false;
            // 
            // chkAdmin
            // 
            this.chkAdmin.AutoSize = true;
            this.chkAdmin.BackColor = System.Drawing.Color.Transparent;
            this.chkAdmin.Font = new System.Drawing.Font("Copperplate Gothic Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAdmin.ForeColor = System.Drawing.Color.Gold;
            this.chkAdmin.Location = new System.Drawing.Point(420, 402);
            this.chkAdmin.Name = "chkAdmin";
            this.chkAdmin.Size = new System.Drawing.Size(135, 19);
            this.chkAdmin.TabIndex = 224;
            this.chkAdmin.Text = "Administrador";
            this.chkAdmin.UseVisualStyleBackColor = false;
            // 
            // salarioText
            // 
            this.salarioText.Location = new System.Drawing.Point(131, 345);
            this.salarioText.Name = "salarioText";
            this.salarioText.Size = new System.Drawing.Size(174, 20);
            this.salarioText.TabIndex = 223;
            // 
            // CBCArgo
            // 
            this.CBCArgo.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBCArgo.FormattingEnabled = true;
            this.CBCArgo.Location = new System.Drawing.Point(459, 344);
            this.CBCArgo.Name = "CBCArgo";
            this.CBCArgo.Size = new System.Drawing.Size(209, 21);
            this.CBCArgo.TabIndex = 222;
            // 
            // Emailtext
            // 
            this.Emailtext.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Emailtext.Location = new System.Drawing.Point(131, 195);
            this.Emailtext.Name = "Emailtext";
            this.Emailtext.Size = new System.Drawing.Size(174, 22);
            this.Emailtext.TabIndex = 221;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Orange;
            this.label23.Location = new System.Drawing.Point(69, 196);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(66, 16);
            this.label23.TabIndex = 220;
            this.label23.Text = "E-mail: ";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Orange;
            this.label21.Location = new System.Drawing.Point(397, 278);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 18);
            this.label21.TabIndex = 219;
            this.label21.Text = "Comp:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Numerotext
            // 
            this.Numerotext.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Numerotext.Location = new System.Drawing.Point(131, 278);
            this.Numerotext.Name = "Numerotext";
            this.Numerotext.Size = new System.Drawing.Size(174, 22);
            this.Numerotext.TabIndex = 218;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label20.ForeColor = System.Drawing.Color.Gold;
            this.label20.Location = new System.Drawing.Point(191, 218);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 18);
            this.label20.TabIndex = 217;
            this.label20.Text = "Endereço";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Gold;
            this.label19.Location = new System.Drawing.Point(191, 70);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(120, 18);
            this.label19.TabIndex = 216;
            this.label19.Text = "Funcionário";
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Orange;
            this.pictureBox8.Location = new System.Drawing.Point(-8, 79);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(700, 4);
            this.pictureBox8.TabIndex = 215;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Orange;
            this.pictureBox7.Location = new System.Drawing.Point(-8, 318);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(700, 4);
            this.pictureBox7.TabIndex = 214;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Orange;
            this.pictureBox6.Location = new System.Drawing.Point(-8, 227);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(700, 4);
            this.pictureBox6.TabIndex = 213;
            this.pictureBox6.TabStop = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Maroon;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Copperplate Gothic Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.Orange;
            this.button5.Location = new System.Drawing.Point(720, 311);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(96, 53);
            this.button5.TabIndex = 211;
            this.button5.Text = "Alterar";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // lblPermissão
            // 
            this.lblPermissão.AutoSize = true;
            this.lblPermissão.BackColor = System.Drawing.Color.Transparent;
            this.lblPermissão.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPermissão.ForeColor = System.Drawing.Color.Orange;
            this.lblPermissão.Location = new System.Drawing.Point(312, 405);
            this.lblPermissão.Name = "lblPermissão";
            this.lblPermissão.Size = new System.Drawing.Size(108, 16);
            this.lblPermissão.TabIndex = 210;
            this.lblPermissão.Text = "Permissões:";
            // 
            // senhaText
            // 
            this.senhaText.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.senhaText.Location = new System.Drawing.Point(131, 399);
            this.senhaText.Name = "senhaText";
            this.senhaText.Size = new System.Drawing.Size(174, 22);
            this.senhaText.TabIndex = 209;
            // 
            // Usuariotext
            // 
            this.Usuariotext.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Usuariotext.Location = new System.Drawing.Point(131, 370);
            this.Usuariotext.Name = "Usuariotext";
            this.Usuariotext.Size = new System.Drawing.Size(537, 22);
            this.Usuariotext.TabIndex = 208;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Orange;
            this.label2.Location = new System.Drawing.Point(54, 373);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 16);
            this.label2.TabIndex = 207;
            this.label2.Text = "Úsuario:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Orange;
            this.label16.Location = new System.Drawing.Point(65, 400);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 16);
            this.label16.TabIndex = 206;
            this.label16.Text = "Senha:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Orange;
            this.label14.Location = new System.Drawing.Point(389, 347);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 16);
            this.label14.TabIndex = 205;
            this.label14.Text = "Cargo:";
            // 
            // DTPNascimento
            // 
            this.DTPNascimento.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPNascimento.Location = new System.Drawing.Point(459, 193);
            this.DTPNascimento.MaxDate = new System.DateTime(2002, 12, 31, 0, 0, 0, 0);
            this.DTPNascimento.Name = "DTPNascimento";
            this.DTPNascimento.Size = new System.Drawing.Size(209, 22);
            this.DTPNascimento.TabIndex = 204;
            this.DTPNascimento.Value = new System.DateTime(2000, 12, 31, 0, 0, 0, 0);
            // 
            // EndercoText
            // 
            this.EndercoText.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EndercoText.Location = new System.Drawing.Point(459, 252);
            this.EndercoText.Name = "EndercoText";
            this.EndercoText.Size = new System.Drawing.Size(209, 22);
            this.EndercoText.TabIndex = 203;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Orange;
            this.label10.Location = new System.Drawing.Point(51, 278);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 16);
            this.label10.TabIndex = 202;
            this.label10.Text = "Número:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Orange;
            this.label13.Location = new System.Drawing.Point(56, 346);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 16);
            this.label13.TabIndex = 201;
            this.label13.Text = "Salário: ";
            // 
            // imgFuncionario
            // 
            this.imgFuncionario.BackColor = System.Drawing.SystemColors.Control;
            this.imgFuncionario.Location = new System.Drawing.Point(703, 93);
            this.imgFuncionario.Name = "imgFuncionario";
            this.imgFuncionario.Size = new System.Drawing.Size(117, 124);
            this.imgFuncionario.TabIndex = 200;
            this.imgFuncionario.TabStop = false;
            this.imgFuncionario.Click += new System.EventHandler(this.imgFuncionario_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Orange;
            this.label11.Location = new System.Drawing.Point(720, 72);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 18);
            this.label11.TabIndex = 199;
            this.label11.Text = "Foto 3x4";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Orange;
            this.pictureBox2.Location = new System.Drawing.Point(688, 51);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(5, 507);
            this.pictureBox2.TabIndex = 198;
            this.pictureBox2.TabStop = false;
            // 
            // CEPtext
            // 
            this.CEPtext.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            this.CEPtext.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CEPtext.Location = new System.Drawing.Point(131, 250);
            this.CEPtext.Name = "CEPtext";
            this.CEPtext.Size = new System.Drawing.Size(174, 22);
            this.CEPtext.TabIndex = 197;
            // 
            // ComplementoText
            // 
            this.ComplementoText.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComplementoText.Location = new System.Drawing.Point(459, 278);
            this.ComplementoText.Name = "ComplementoText";
            this.ComplementoText.Size = new System.Drawing.Size(209, 22);
            this.ComplementoText.TabIndex = 196;
            // 
            // RGtext
            // 
            this.RGtext.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RGtext.Location = new System.Drawing.Point(459, 141);
            this.RGtext.Mask = "00.000.000-0";
            this.RGtext.Name = "RGtext";
            this.RGtext.Size = new System.Drawing.Size(209, 22);
            this.RGtext.TabIndex = 195;
            // 
            // CPFtext
            // 
            this.CPFtext.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CPFtext.Location = new System.Drawing.Point(131, 138);
            this.CPFtext.Mask = "000.000.000-00";
            this.CPFtext.Name = "CPFtext";
            this.CPFtext.Size = new System.Drawing.Size(174, 22);
            this.CPFtext.TabIndex = 194;
            // 
            // Celtext
            // 
            this.Celtext.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Celtext.Location = new System.Drawing.Point(459, 167);
            this.Celtext.Mask = "(00) 00000-0000";
            this.Celtext.Name = "Celtext";
            this.Celtext.Size = new System.Drawing.Size(209, 22);
            this.Celtext.TabIndex = 193;
            // 
            // Teltext
            // 
            this.Teltext.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Teltext.Location = new System.Drawing.Point(131, 164);
            this.Teltext.Mask = "(00) 0000-0000";
            this.Teltext.Name = "Teltext";
            this.Teltext.Size = new System.Drawing.Size(174, 22);
            this.Teltext.TabIndex = 192;
            // 
            // NomeText
            // 
            this.NomeText.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NomeText.Location = new System.Drawing.Point(131, 111);
            this.NomeText.Name = "NomeText";
            this.NomeText.Size = new System.Drawing.Size(537, 22);
            this.NomeText.TabIndex = 191;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Orange;
            this.label9.Location = new System.Drawing.Point(342, 254);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(115, 18);
            this.label9.TabIndex = 190;
            this.label9.Text = "Rua/Aveni.:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Orange;
            this.label3.Location = new System.Drawing.Point(311, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(146, 18);
            this.label3.TabIndex = 189;
            this.label3.Text = "Dt.Nascimento:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Orange;
            this.label7.Location = new System.Drawing.Point(69, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 16);
            this.label7.TabIndex = 188;
            this.label7.Text = "Nome: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Orange;
            this.label6.Location = new System.Drawing.Point(80, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 16);
            this.label6.TabIndex = 187;
            this.label6.Text = "CPF:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Orange;
            this.label5.Location = new System.Drawing.Point(418, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 18);
            this.label5.TabIndex = 186;
            this.label5.Text = "RG: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Orange;
            this.label4.Location = new System.Drawing.Point(39, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 16);
            this.label4.TabIndex = 185;
            this.label4.Text = "Telefone: ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Orange;
            this.label12.Location = new System.Drawing.Point(374, 168);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 18);
            this.label12.TabIndex = 184;
            this.label12.Text = "Celular: ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Orange;
            this.label15.Location = new System.Drawing.Point(80, 252);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 16);
            this.label15.TabIndex = 183;
            this.label15.Text = "CEP:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Maroon;
            this.label8.Font = new System.Drawing.Font("Copperplate Gothic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Gold;
            this.label8.Location = new System.Drawing.Point(243, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(296, 26);
            this.label8.TabIndex = 180;
            this.label8.Text = "Alterar funcionário";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Maroon;
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Location = new System.Drawing.Point(-1, 7);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(18, 16);
            this.pictureBox4.TabIndex = 179;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Orange;
            this.pictureBox1.Location = new System.Drawing.Point(-22, 43);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(855, 10);
            this.pictureBox1.TabIndex = 178;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Maroon;
            this.pictureBox3.Location = new System.Drawing.Point(-8, -6);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(852, 51);
            this.pictureBox3.TabIndex = 177;
            this.pictureBox3.TabStop = false;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.LinkColor = System.Drawing.Color.Orange;
            this.linkLabel1.Location = new System.Drawing.Point(-4, 14);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(34, 13);
            this.linkLabel1.TabIndex = 176;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Voltar";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gold;
            this.label1.Location = new System.Drawing.Point(148, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(316, 31);
            this.label1.TabIndex = 175;
            this.label1.Text = "Cadastro De Funcionário";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.DarkRed;
            this.label22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label22.Font = new System.Drawing.Font("Copperplate Gothic Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Gold;
            this.label22.Location = new System.Drawing.Point(790, 8);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(26, 24);
            this.label22.TabIndex = 232;
            this.label22.Text = "X";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.DarkRed;
            this.label17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label17.Font = new System.Drawing.Font("Copperplate Gothic Bold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Gold;
            this.label17.Location = new System.Drawing.Point(753, -6);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 35);
            this.label17.TabIndex = 233;
            this.label17.Text = "_";
            // 
            // BuscarCep
            // 
            this.BuscarCep.BackColor = System.Drawing.Color.Transparent;
            this.BuscarCep.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BuscarCep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BuscarCep.Font = new System.Drawing.Font("Copperplate Gothic Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BuscarCep.ForeColor = System.Drawing.Color.Orange;
            this.BuscarCep.Location = new System.Drawing.Point(311, 247);
            this.BuscarCep.Name = "BuscarCep";
            this.BuscarCep.Size = new System.Drawing.Size(25, 26);
            this.BuscarCep.TabIndex = 294;
            this.BuscarCep.Text = "+";
            this.BuscarCep.UseVisualStyleBackColor = false;
            this.BuscarCep.Click += new System.EventHandler(this.BuscarCep_Click);
            // 
            // AlterarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(823, 552);
            this.Controls.Add(this.BuscarCep);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.chkCompras);
            this.Controls.Add(this.chkVendas);
            this.Controls.Add(this.chkFinanceiro);
            this.Controls.Add(this.chkRH);
            this.Controls.Add(this.chkLogistica);
            this.Controls.Add(this.chkEmpregado);
            this.Controls.Add(this.chkAdmin);
            this.Controls.Add(this.salarioText);
            this.Controls.Add(this.CBCArgo);
            this.Controls.Add(this.Emailtext);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Numerotext);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.lblPermissão);
            this.Controls.Add(this.senhaText);
            this.Controls.Add(this.Usuariotext);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.DTPNascimento);
            this.Controls.Add(this.EndercoText);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.imgFuncionario);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.CEPtext);
            this.Controls.Add(this.ComplementoText);
            this.Controls.Add(this.RGtext);
            this.Controls.Add(this.CPFtext);
            this.Controls.Add(this.Celtext);
            this.Controls.Add(this.Teltext);
            this.Controls.Add(this.NomeText);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AlterarFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AlterarFuncionario";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.CheckBox chkCompras;
        private System.Windows.Forms.CheckBox chkVendas;
        private System.Windows.Forms.CheckBox chkFinanceiro;
        private System.Windows.Forms.CheckBox chkRH;
        private System.Windows.Forms.CheckBox chkLogistica;
        private System.Windows.Forms.CheckBox chkEmpregado;
        private System.Windows.Forms.CheckBox chkAdmin;
        private System.Windows.Forms.TextBox salarioText;
        private System.Windows.Forms.ComboBox CBCArgo;
        private System.Windows.Forms.MaskedTextBox Emailtext;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.MaskedTextBox Numerotext;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label lblPermissão;
        private System.Windows.Forms.MaskedTextBox senhaText;
        private System.Windows.Forms.MaskedTextBox Usuariotext;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker DTPNascimento;
        private System.Windows.Forms.MaskedTextBox EndercoText;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox imgFuncionario;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.MaskedTextBox CEPtext;
        private System.Windows.Forms.MaskedTextBox ComplementoText;
        private System.Windows.Forms.MaskedTextBox RGtext;
        private System.Windows.Forms.MaskedTextBox CPFtext;
        private System.Windows.Forms.MaskedTextBox Celtext;
        private System.Windows.Forms.MaskedTextBox Teltext;
        private System.Windows.Forms.MaskedTextBox NomeText;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button BuscarCep;
    }
}