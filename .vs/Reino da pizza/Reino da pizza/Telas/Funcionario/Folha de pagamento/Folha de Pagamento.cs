﻿using Reino_da_pizza.DB.Folha_de_pagamento;
using Reino_da_pizza.DB.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class Folha_de_Pagamento : Form
    {
        //private object lblnome;

        public Folha_de_Pagamento()
        {
            InitializeComponent();
            CarregarCombos();
            CarregarMes();
            lblnome.Visible = false;

            cboFuncionario.SelectedText = "Selecione o funcionário";
            cboMes.SelectedText = "Selecione o mês";
        }

        private void CarregarMes()
        {
            List<string> mes = new List<string>();
            mes.Add("Selecione o mês");
            mes.Add("Janeiro");
            mes.Add("Fevereiro");
            mes.Add("Março");
            mes.Add("Abril");
            mes.Add("Maio");
            mes.Add("Junho");
            mes.Add("Julho");
            mes.Add("Agosto");
            mes.Add("Setembro");
            mes.Add("Outubro");
            mes.Add("Novembro");
            mes.Add("Dezembro");

            cboMes.DataSource = mes;
        }




        void CarregarCombos()
        {


            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> listar = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.ID);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboFuncionario.DataSource = listar;




        }
        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Folha_de_Pagamento_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void Folha_de_Pagamento_Load_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {


                FuncionarioDTO funcionario = new FuncionarioDTO();

                FolhaPagamentoDTO folha = new FolhaPagamentoDTO();


                folha.Nome = lblnome.Text;
                folha.Extra50 = Convert.ToDecimal(lblExtra50.Text);
                folha.Extra100 = Convert.ToDecimal(lblExtra100.Text);
                folha.Atrasos = Convert.ToDecimal(lblAtrasos.Text);
                folha.Faltas = Convert.ToDecimal(lblFaltas.Text);
                folha.SalarioBruto = Convert.ToDecimal(lblSalarioBruto.Text);
                folha.Mes = cboMes.SelectedItem.ToString();
                folha.DSR = Convert.ToDecimal(lblDSR.Text);
                folha.INSS = Convert.ToDecimal(lblINSS.Text);
                folha.ImpostoRenda = Convert.ToDecimal(lblImpostoRenda.Text);
                folha.ValeTransporte = Convert.ToDecimal(lblVT.Text);
                folha.FGTS = Convert.ToDecimal(lblFGTS.Text);
                folha.SalarioLiquido = Convert.ToDecimal(lblSalarioL.Text);


                FolhaPagamentoBusiness business = new FolhaPagamentoBusiness();
                business.Salvar(folha);


                EnviarMensagem("Folha de pagamento salva com sucesso");
                EnviarMensagemNovo("Desejar fazer outra folha de pagamento ?");
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possível salvar a folha de pagamento: " + ex.Message);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {

                decimal salarioBruto = Convert.ToDecimal(lblSalarioBruto.Text);

                  int horas50 = 0;
                  int atrasos = 0;
                  int faltas = 0;
                  int domingos = 0;
                  int horas100 = 0;

                  if (txtExtra50.Text == "")
                  {
                      horas50 = 0;
                  }
                  else
                  {
                      horas50 = Convert.ToInt32(txtExtra50.Text);
                  }
                  if (txtExtra100.Text == "")
                  {
                      horas100 = 0;
                  }
                  else
                  {
                      horas100 = Convert.ToInt32(txtExtra100.Text);
                  }
                  if (txtAtrasos.Text == "")
                  {
                      atrasos = 0;
                  }
                  else
                  {
                      atrasos = Convert.ToInt32(txtAtrasos.Text);
                  }
                  if (txtFaltas.Text == "")
                  {
                      faltas = 0;
                  }
                  else
                  {
                      faltas = Convert.ToInt32(txtFaltas.Text);
                  }
                  if (txtDomingos.Text == "")
                  {
                      domingos = 0;
                  }
                  else
                  {
                      domingos = Convert.ToInt32(txtDomingos.Text);
                  }


                






                FolhaPagamentoOBJ folha = new FolhaPagamentoOBJ();
                decimal Salariohora = folha.SalarioHora(salarioBruto);

                decimal VT = folha.ValeTransporte(salarioBruto);
                VT = decimal.Round(VT, 2);

                lblVT.Text = Convert.ToString(VT);

                decimal Extra50 = folha.HoraExtra50(salarioBruto, horas50);
                Extra50 = decimal.Round(Extra50, 2);
                lblExtra50.Text = Convert.ToString(Extra50);

                decimal Extra100 = folha.HoraExtra100(salarioBruto, horas100);
                Extra100 = decimal.Round(Extra100, 2);
                lblExtra100.Text = Convert.ToString(Extra100);

                decimal hrExtra = Extra50 + Extra100;

                decimal atraso = folha.CalcularAtrasos(Salariohora, atrasos);
                atraso = decimal.Round(atraso, 2);
                lblAtrasos.Text = Convert.ToString(atraso);

                decimal falta = folha.CalcularFalta(salarioBruto, faltas, domingos);
                falta = decimal.Round(falta, 2);
                lblFaltas.Text = Convert.ToString(falta);

                decimal dsr = folha.CalcularDSR(hrExtra, domingos, faltas);
                dsr = decimal.Round(dsr, 2);
                lblDSR.Text = Convert.ToString(dsr);



                decimal inss = folha.CalculoINSS(salarioBruto, atraso, hrExtra, dsr);
                inss = decimal.Round(inss, 2);
                lblINSS.Text = Convert.ToString(inss);

                decimal ImpostoRenda = folha.CalculoImpostoRenda(salarioBruto, atraso, hrExtra, dsr, inss);
                ImpostoRenda = decimal.Round(ImpostoRenda, 2);
                lblImpostoRenda.Text = Convert.ToString(ImpostoRenda);

                decimal fgts = folha.CalculoFGTS(salarioBruto, Extra50, Extra100);
                fgts = decimal.Round(fgts, 2);
                lblFGTS.Text = Convert.ToString(fgts);


                lblSalarioL.Text = Convert.ToString((salarioBruto + dsr + hrExtra) - (falta + atraso + VT + inss + ImpostoRenda));

            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possivel calcular a folha de pagamento: " + ex.Message);
            }

        }
        public void Limpar()
        {
            txtAtrasos.Text = "";
            txtDomingos.Text = "";
            txtExtra100.Text = "";
            txtExtra50.Text = "";
            txtFaltas.Text = "";

            lblExtra50.Text = "R$ 0,00";
            lblExtra100.Text = "R$ 0,00";
            lblAtrasos.Text = "R$ 0,00";
            lblFaltas.Text = "R$ 0,00";

            cboMes.SelectedItem = "Selecione o mês";
            lblDSR.Text = "R$ 0,00";
            lblINSS.Text = "R$ 0,00";
            lblImpostoRenda.Text = "R$ 0,00";
            lblVT.Text = "R$ 0,00";
            lblFGTS.Text = "R$ 0,00";
            lblSalarioL.Text = "R$ 0,00"; 
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da Pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }

        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da Pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }


        private void button3_Click(object sender, EventArgs e)
        {
            txtAtrasos.Text = "";
            txtDomingos.Text = "";
            txtExtra100.Text = "";
            txtExtra50.Text = "";
            txtFaltas.Text = "";

            lblExtra50.Text = "R$ 0,00";
            lblExtra100.Text = "R$ 0,00";
            lblAtrasos.Text = "R$ 0,00";
            lblFaltas.Text = "R$ 0,00";

            cboMes.SelectedItem = "Selecione o mês";
            lblDSR.Text = "R$ 0,00";
            lblINSS.Text = "R$ 0,00";
            lblImpostoRenda.Text = "R$ 0,00";
            lblVT.Text = "R$ 0,00";
            lblFGTS.Text = "R$ 0,00";
            lblSalarioL.Text = "R$ 0,00";
        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void cboFuncionario_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            FuncionarioDTO func = cboFuncionario.SelectedItem as FuncionarioDTO;

            lblSalarioBruto.Text = Convert.ToString(func.Salario);
            lblnome.Text = Convert.ToString(func.Nome);
        }

        private void pictureBox9_Click_1(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }
    }
    
}
