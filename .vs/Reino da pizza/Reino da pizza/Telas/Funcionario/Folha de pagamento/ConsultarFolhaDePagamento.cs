﻿using Reino_da_pizza.DB.Folha_de_pagamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza.Telas
{
    public partial class ConsultarFolhaDePagamento : Form
    {
        public ConsultarFolhaDePagamento()
        {
            InitializeComponent();
        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Reino da pizza",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Reino da pizza",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        void CarregarGrid()
        {

            string nome = txtNome.Text;
            FolhaPagamentoBusiness Business = new FolhaPagamentoBusiness();
            List<FolhaPagamentoDTO> lista = Business.Consultar();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CarregarGrid();
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possivel realizar a consulta: " + ex.Message);
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                FolhaPagamentoDTO funcionario = dataGridView1.CurrentRow.DataBoundItem as FolhaPagamentoDTO;

                DialogResult r = MessageBox.Show("Deseja excluir a folha de pagamento?", "Reino da pizza",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FolhaPagamentoBusiness business = new FolhaPagamentoBusiness();
                    business.Remover(funcionario.Id);

                    CarregarGrid();
                }
            }
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label15_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox5_Click_1(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }
    }
    }

