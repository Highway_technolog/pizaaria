﻿using Reino_da_pizza.DB.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class ConsultarFuncionário : Form
    {
        public ConsultarFuncionário()
        {
            InitializeComponent();
        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        void CarregarGrid()
        {

            string nome = txtNome.Text;
            FuncionarioBusiness Business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = Business.Consultar(nome);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
