﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Folha_de_pagamento
{
    class FolhaPagamentoBusiness
    {

        FolhaPagamentoDatabase db = new FolhaPagamentoDatabase();

        public int Salvar(FolhaPagamentoDTO funcionario)
        {

            return db.Salvar(funcionario);
        }

        /* public void Alterar(FolhaPagamentoDTO funcionario)
         {


             db.Alterar(funcionario);
         }*/

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<FolhaPagamentoDTO> Consultar()
        {

            return db.Consultar();
        }



        public List<FolhaPagamentoDTO> Listar()
        {
            FolhaPagamentoDatabase db = new FolhaPagamentoDatabase();
            return db.Listar();
        }
    }
}
