﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Folha_de_pagamento
{
    public class FolhaPagamentoDTO
    {
        public int Id { get; set; }
        public decimal Atrasos { get; set; }
        public decimal Faltas { get; set; }
        public decimal INSS { get; set; }
        public decimal SalarioBruto { get; set; }
        public decimal ImpostoRenda { get; set; }
        public decimal SalarioLiquido { get; set; }
        public decimal FGTS { get; set; }
        public decimal ValeTransporte { get; set; }
        public decimal DSR { get; set; }
        public decimal Extra50 { get; set; }
        public decimal Extra100 { get; set; }
        public string Nome { get; set; }
        public string Mes { get; set; }




    }
}
