﻿using MySql.Data.MySqlClient;
using Reino_da_pizza.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Folha_de_pagamento
{
    class FolhaPagamentoDatabase
    {
        public int Salvar(FolhaPagamentoDTO folhaPagamento)
        {
            string script =
            @"INSERT INTO TB_FOLHAPAGAMENTO
            (
	            HR_ATRASOS,                                    
                DS_FALTAS,
                DS_INSS,
                VL_SALARIO_BRUTO,
                VL_SALARIO_LIQUIDO,
                VL_FGTS,
                VL_VT,
                DS_DSR,
                VL_HE_50,
                VL_HE_100,
                NM_FUNCIONARIO,
                DS_MES,
                DS_IMPOSTO_RENDA
            )
            VALUES
            (
	            @HR_ATRASOS,                                    
                @DS_FALTAS,
                @DS_INSS,
                @VL_SALARIO_BRUTO,
                @VL_SALARIO_LIQUIDO,
                @VL_FGTS,
                @VL_VT,
                @DS_DSR,
                @VL_HE_50,
                @VL_HE_100,
                @NM_FUNCIONARIO,
                @DS_MES,
                @DS_IMPOSTO_RENDA
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("hr_atrasos", folhaPagamento.Atrasos));
            parms.Add(new MySqlParameter("ds_faltas", folhaPagamento.Faltas));
            parms.Add(new MySqlParameter("ds_inss", folhaPagamento.INSS));
            parms.Add(new MySqlParameter("vl_salario_bruto", folhaPagamento.SalarioBruto));
            parms.Add(new MySqlParameter("ds_imposto_renda", folhaPagamento.ImpostoRenda));
            parms.Add(new MySqlParameter("vl_salario_liquido", folhaPagamento.SalarioLiquido));
            parms.Add(new MySqlParameter("vl_fgts", folhaPagamento.FGTS));
            parms.Add(new MySqlParameter("vl_vt", folhaPagamento.ValeTransporte));
            parms.Add(new MySqlParameter("ds_dsr", folhaPagamento.DSR));
            parms.Add(new MySqlParameter("vl_he_50", folhaPagamento.Extra50));
            parms.Add(new MySqlParameter("vl_he_100", folhaPagamento.Extra100));
            parms.Add(new MySqlParameter("NM_FUNCIONARIO", folhaPagamento.Nome));
            parms.Add(new MySqlParameter("ds_mes", folhaPagamento.Mes));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        /* public void Alterar(FolhaPagamentoDTO folhaPagamento)
         {
             string script =
             @"UPDATE tb_cliente 
                  SET  hr_atrasos             =  @hr_atrasos                             
                       ds_faltas   =  @ds_faltas
                       ds_inss =  @ds_inss
                       vl_salario  =  @vl_salario
                       vl_desconto  = @vl_desconto
                       vl_salario_liquido  =  @vl_salario_liquido
                       vl_fgts =  @vl_fgts
                       vl_vt= @vl_vt
                       ds_dsr  =  @ds_dsr
                       ds_he_50 = @ds_he_50
                       vl_he_100 = @vl_he_100
                       id_funcionario =   @id_funcionario
                       ds_mes  =  @ds_mes
                WHERE id_folha_pagamento = @id_folha_pagamento";

             List<MySqlParameter> parms = new List<MySqlParameter>();
             parms.Add(new MySqlParameter("id_folha_pagamento", folhaPagamento.Id));
             parms.Add(new MySqlParameter("hr_atrasos", folhaPagamento.Atrasos));
             parms.Add(new MySqlParameter("ds_atrasos", folhaPagamento.Faltas));
             parms.Add(new MySqlParameter("ds_inss", folhaPagamento.INSS));
             parms.Add(new MySqlParameter("vl_salario", folhaPagamento.SalarioBruto));
             parms.Add(new MySqlParameter("vl_desconto", folhaPagamento.Desconto));
             parms.Add(new MySqlParameter("vl_salario_liquido", folhaPagamento.SalarioLiquido));
             parms.Add(new MySqlParameter("vl_fgts", folhaPagamento.FGTS));
             parms.Add(new MySqlParameter("vl_vt", folhaPagamento.ValeTransporte));
             parms.Add(new MySqlParameter("ds_dsr", folhaPagamento.DSR));
             parms.Add(new MySqlParameter("vl_he_50", folhaPagamento.Extra50));
             parms.Add(new MySqlParameter("vl_he_100", folhaPagamento.Extra100));
             parms.Add(new MySqlParameter("id_funcionario", folhaPagamento.IdFuncionario));
             parms.Add(new MySqlParameter("ds_mes", folhaPagamento.Mes));


             Database db = new Database();
             db.ExecuteInsertScript(script, parms);
         }*/

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_FOLHAPAGAMENTO WHERE id_folha_pagamento = @id_folha_pagamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_folha_pagamento", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<FolhaPagamentoDTO> Consultar()
        {
            string script =
            @"SELECT * 
                FROM TB_FOLHAPAGAMENTO
               ";

            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FolhaPagamentoDTO> FolhaPag = new List<FolhaPagamentoDTO>();

            while (reader.Read())
            {
                FolhaPagamentoDTO folha = new FolhaPagamentoDTO();
                folha.Id = reader.GetInt32("id_folha_pagamento");
                folha.Atrasos = reader.GetDecimal("hr_atrasos");
                folha.Faltas = reader.GetDecimal("ds_faltas");
                folha.INSS = reader.GetDecimal("ds_inss");
                folha.SalarioBruto = reader.GetDecimal("vl_salario_bruto");
                folha.ImpostoRenda = reader.GetDecimal("ds_imposto_renda");
                folha.SalarioLiquido = reader.GetDecimal("vl_salario_liquido");
                folha.FGTS = reader.GetDecimal("vl_fgts");
                folha.ValeTransporte = reader.GetDecimal("vl_vt");
                folha.DSR = reader.GetDecimal("ds_dsr");
                folha.Extra50 = reader.GetDecimal("vl_he_50");
                folha.Extra100 = reader.GetDecimal("vl_he_100");
                folha.Nome = reader.GetString("nm_funcionario");
                folha.Mes = reader.GetString("ds_mes");

                FolhaPag.Add(folha);
            }
            reader.Close();

            return FolhaPag;
        }

        public List<FolhaPagamentoDTO> Listar()
        {
            string script = @"SELECT * FROM TB_FOLHAPAGAMENTO";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FolhaPagamentoDTO> lista = new List<FolhaPagamentoDTO>();
            while (reader.Read())
            {

                FolhaPagamentoDTO folha = new FolhaPagamentoDTO();
                folha.Id = reader.GetInt32("id_folha_pagamento");
                folha.Atrasos = reader.GetDecimal("hr_atrasos");
                folha.Faltas = reader.GetDecimal("ds_faltas");
                folha.INSS = reader.GetDecimal("ds_inss");
                folha.SalarioBruto = reader.GetDecimal("vl_salario_bruto");
                folha.ImpostoRenda = reader.GetDecimal("ds_impostorenda");
                folha.SalarioLiquido = reader.GetDecimal("vl_salario_liquido");
                folha.FGTS = reader.GetDecimal("vl_fgts");
                folha.ValeTransporte = reader.GetDecimal("vl_vt");
                folha.DSR = reader.GetDecimal("ds_dsr");
                folha.Extra50 = reader.GetDecimal("vl_he_50");
                folha.Extra100 = reader.GetDecimal("vl_he_100");
                folha.Nome = reader.GetString("nm_funcionario");
                folha.Mes = reader.GetString("ds_mes");


                lista.Add(folha);
            }
            reader.Close();

            return lista;

        }

    }
}
