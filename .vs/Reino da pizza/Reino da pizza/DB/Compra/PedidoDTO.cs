﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Fornecedor
{
    public class PedidoDTO
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public decimal Valor { get; set; }
        public int IdFornecedor { get; set; }
        public string NomeFornecedor { get; set; }
    }
}
