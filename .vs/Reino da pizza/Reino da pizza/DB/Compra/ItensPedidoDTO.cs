﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Fornecedor
{
   public class ItensPedidoDTO
    {
        public int Id { get; set; }
        public int Quantidade { get; set; }
        public decimal PrecoVenda { get; set; }
        public int IdPedido { get; set; }
        public int IdProduto { get; set; }
    }
}
