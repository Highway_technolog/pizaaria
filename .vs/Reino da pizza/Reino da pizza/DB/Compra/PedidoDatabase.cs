﻿using MySql.Data.MySqlClient;
using Reino_da_pizza.DB.Base;
using Reino_da_pizza.DB.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Fornecedor
{
    class PedidoDatabase
    {
        public int Salvar(PedidoDTO pedido)
        {
            string script =
            @"INSERT INTO TB_PEDIDO
            (
	            DT_DATA,                                    
                DS_VALOR,
                ID_FORNECEDOR,
                NM_FORNECEDOR
                
            )
            VALUES
            (
	            @DT_DATA,                                    
                @DS_VALOR,
                @ID_FORNECEDOR,
                @NM_FORNECEDOR
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DT_DATA", pedido.Data));
            parms.Add(new MySqlParameter("DS_VALOR", pedido.Valor));
            parms.Add(new MySqlParameter("ID_FORNECEDOR", pedido.IdFornecedor));
            parms.Add(new MySqlParameter("NM_FORNECEDOR", pedido.NomeFornecedor));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        /*public void Alterar(ProdutoDTO produto)
        {
            string script =
            @"UPDATE TB_PRODUTO
                 SET    NM_PRODUTO        =   @NM_PRODUTO,                              
                        DS_PRECOUNITARIO  =   @DS_PRECOUNITARIO,
                        DS_TIPO           =   @DS_TIPO,
                        ID_FORNECEDOR     =   @ID_FORNECEDOR,
                        DS_FOTO           =   @DS_FOTO,
                        NM_FORNECEDOR     =   @NM_FORNECEDOR
               WHERE ID_PRODUTO = @ID_PRODUTO";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_PRODUTO", produto.Nome));
            parms.Add(new MySqlParameter("DS_PRECOUNITARIO", produto.ValorUnitario));
            parms.Add(new MySqlParameter("DS_TIPO", produto.Tipo));
            parms.Add(new MySqlParameter("ID_FORNECEDOR", produto.IdFornecedor));
            parms.Add(new MySqlParameter("DS_FOTO", produto.Foto));
            parms.Add(new MySqlParameter("NM_FORNECEDOR", produto.FornecedorNome));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }*/

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_PEDIDO WHERE ID_PEDIDO = @ID_PEDIDO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PEDIDO", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<PedidoDTO> Consultar(DateTime inicio, DateTime fim)
        {
            string script =
            @"SELECT * 
                FROM TB_PEDIDO
                 WHERE DT_DATA >= @inicio and DT_DATA <=@fim
               ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("inicio", inicio));
            parms.Add(new MySqlParameter("fim", fim));


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);


            List<PedidoDTO> pedido = new List<PedidoDTO>();

            while (reader.Read())
            {
                PedidoDTO novoProduto = new PedidoDTO();
                novoProduto.Id = reader.GetInt32("ID_PEDIDO");
                novoProduto.Data = reader.GetDateTime("DT_DATA");
                novoProduto.IdFornecedor = reader.GetInt32("ID_FORNECEDOR");
                novoProduto.Valor = reader.GetDecimal("DS_VALOR");
                novoProduto.NomeFornecedor = reader.GetString("NM_FORNECEDOR");


                pedido.Add(novoProduto);
            }
            reader.Close();

            return pedido;
        }

        public List<VwPedidoDTO> ListarCompra()
        {
            string script = @"SELECT * 
                            FROM Vw_pedido
                             ;";
            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<VwPedidoDTO> pedido = new List<VwPedidoDTO>();

            while (reader.Read())
            {
                VwPedidoDTO novoProduto = new VwPedidoDTO();
                novoProduto.Produto = reader.GetString("NM_PRODUTO");
                novoProduto.Data = reader.GetDateTime("DT_DATA");
                novoProduto.Fornecedor = reader.GetString("NM_NOME");
                novoProduto.Valor = reader.GetDecimal("DS_VALOR");



                pedido.Add(novoProduto);
            }
            reader.Close();
            return pedido;

            /*create View Vw_pedido as
            select TB_PEDIDO.DT_DATA,TB_PEDIDO.DS_VALOR,TB_FORNECEDOR.NM_NOME,TB_PRODUTO.NM_PRODUTO
            FROM TB_PEDIDO
            INNER JOIN TB_ItensPEDIDO
            ON TB_PEDIDO.ID_PEDIDO = TB_ItensPEDIDO.ID_PEDIDO
            INNER JOIN TB_FORNECEDOR
            ON TB_PEDIDO.ID_PEDIDO = TB_FORNECEDOR.ID_FORNECEDOR
            INNER JOIN TB_PRODUTO
            ON TB_ItensPEDIDO.ID_PRODUTO = TB_PRODUTO.ID_PRODUTO;*/



        }
    }
}
