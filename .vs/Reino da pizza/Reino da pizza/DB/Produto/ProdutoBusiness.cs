﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Produto
{
    class ProdutoBusiness
    {
        ProdutoDatabase db = new ProdutoDatabase();

        public int Salvar(ProduçaoDTO nome)
        {
            if (nome.Nome == "")
            {
                throw new ArgumentException("Nome é obrigatorio.");
            }
            if (nome.ValorUnitario == 0)
            {
                throw new ArgumentException("Defina o preço para o produto.");
            }

            return db.Salvar(nome);
        }

        public void Alterar(ProduçaoDTO nome)
        {
            if (nome.Nome == "")
            {
                throw new ArgumentException("Nome é obrigatorio.");
            }
            if (nome.ValorUnitario == 0)
            {
                throw new ArgumentException("Defina um valor para o produto");
            }



            db.Alterar(nome);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<ProduçaoDTO> Consultar()
        {

            return db.Consultar();
        }



        public List<ProduçaoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }
    }
}
