﻿using MySql.Data.MySqlClient;
using Reino_da_pizza.DB.Base;
using Reino_da_pizza.DB.Funcionário;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProduçaoDTO produto)
        {
            string script =
            @"INSERT INTO TB_PRODUTO
            (
	            NM_PRODUTO,                                    
                DS_PRECOUNITARIO,
                DS_TIPO,
                ID_FORNECEDOR,
                DS_FOTO,
                NM_FORNECEDOR
                
            )
            VALUES
            (
	            @NM_PRODUTO,                                    
                @DS_PRECOUNITARIO,
                @DS_TIPO,
                @ID_FORNECEDOR,
                @DS_FOTO,
                @NM_FORNECEDOR
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_PRECOUNITARIO", produto.ValorUnitario));


            parms.Add(new MySqlParameter("DS_TIPO", produto.Tipo));
            parms.Add(new MySqlParameter("ID_FORNECEDOR", produto.IdFornecedor));
            parms.Add(new MySqlParameter("DS_FOTO", produto.Foto));
            parms.Add(new MySqlParameter("NM_PRODUTO", produto.Nome));
            parms.Add(new MySqlParameter("NM_FORNECEDOR", produto.FornecedorNome));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ProduçaoDTO produto)
        {
            string script =
            @"UPDATE TB_PRODUTO
                 SET    NM_PRODUTO        =   @NM_PRODUTO,                              
                        DS_PRECOUNITARIO  =   @DS_PRECOUNITARIO,
                        DS_TIPO           =   @DS_TIPO,
                        ID_FORNECEDOR     =   @ID_FORNECEDOR,
                        DS_FOTO           =   @DS_FOTO,
                        NM_FORNECEDOR     =   @NM_FORNECEDOR
               WHERE ID_PRODUTO = @ID_PRODUTO";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_PRODUTO", produto.Nome));
            parms.Add(new MySqlParameter("DS_PRECOUNITARIO", produto.ValorUnitario));
            parms.Add(new MySqlParameter("DS_TIPO", produto.Tipo));
            parms.Add(new MySqlParameter("ID_FORNECEDOR", produto.IdFornecedor));
            parms.Add(new MySqlParameter("DS_FOTO", produto.Foto));
            parms.Add(new MySqlParameter("NM_FORNECEDOR", produto.FornecedorNome));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_PRODUTO WHERE ID_PRODUTO = @ID_PRODUTO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PRODUTO", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProduçaoDTO> Consultar()
        {
            string script =
            @"SELECT * 
                FROM TB_PRODUTO
               ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProduçaoDTO> produto = new List<ProduçaoDTO>();

            while (reader.Read())
            {
                ProduçaoDTO novoProduto = new ProduçaoDTO();
                novoProduto.Id = reader.GetInt32("ID_PRODUTO");
                novoProduto.Nome = reader.GetString("NM_PRODUTO");
                novoProduto.ValorUnitario = reader.GetDecimal("DS_PRECOUNITARIO");
                novoProduto.Tipo = reader.GetString("DS_TIPO");
                novoProduto.IdFornecedor = reader.GetInt32("ID_FORNECEDOR");
                novoProduto.FornecedorNome = reader.GetString("NM_FORNECEDOR");

                produto.Add(novoProduto);
            }
            reader.Close();

            return produto;
        }

        public List<ProduçaoDTO> Listar()
        {
            string script = @"SELECT * FROM TB_PRODUTO
                               ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProduçaoDTO> lista = new List<ProduçaoDTO>();
            while (reader.Read())
            {

                ProduçaoDTO novoProduto = new ProduçaoDTO();
                novoProduto.Id = reader.GetInt32("ID_PRODUTO");
                novoProduto.Nome = reader.GetString("NM_PRODUTO");
                novoProduto.FornecedorNome = reader.GetString("NM_FORNECEDOR");
                novoProduto.ValorUnitario = reader.GetDecimal("DS_PRECOUNITARIO");
                novoProduto.Tipo = reader.GetString("DS_TIPO");
                novoProduto.IdFornecedor = reader.GetInt32("ID_FORNECEDOR");


                lista.Add(novoProduto);
            }
            reader.Close();

            return lista;
        }
    }
}
