﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Funcionário
{
    class FuncionarioBusiness
    {
        FuncionarioDatabase db = new FuncionarioDatabase();

        public int Salvar(FuncionarioDTO funcionario)
        {
            if (funcionario.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (funcionario.CPF == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório.");
            }
            if (funcionario.RG == string.Empty)
            {
                throw new ArgumentException("RG é obrigatório.");
            }

            if (funcionario.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório.");
            }
            if (funcionario.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório.");
            }

            if (funcionario.Numero == string.Empty)
            {
                throw new ArgumentException("Número é obrigatório.");
            }
            if (funcionario.Endereço == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório.");
            }

            if (funcionario.Salario == 0)
            {
                throw new ArgumentException("Salário é obrigatório.");
            }
            if (funcionario.Cargo == string.Empty)
            {
                throw new ArgumentException("Cargo é obrigatório.");
            }
            if (funcionario.Admin == false && funcionario.Compras == false && funcionario.Empregado == false && funcionario.Financeiro == false && funcionario.Logistica == false && funcionario.RH == false && funcionario.Vendas == false)
            {
                throw new ArgumentException("Marque pelo menos uma permissão.");
            }
            if (funcionario.Usuario == string.Empty)
            {
                throw new ArgumentException("Usuário é obrigatório.");
            }

            if (funcionario.Senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatória.");
            }
            if (funcionario.Cargo == "Selecione o cargo")
            {
                throw new ArgumentException("Selecione um cargo valido");
            }
            string a = funcionario.Email;
            bool b = a.Contains("@");
            if (b == false)
            {
                throw new ArgumentException("Informe um email valido.");
            }
            return db.Salvar(funcionario);

        }

        public void Alterar(FuncionarioDTO funcionario)
        {
            if (funcionario.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (funcionario.CPF == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório.");
            }
            if (funcionario.RG == string.Empty)
            {
                throw new ArgumentException("RG é obrigatório.");
            }

            if (funcionario.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório.");
            }
            if (funcionario.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório.");
            }

            if (funcionario.Numero == string.Empty)
            {
                throw new ArgumentException("Número é obrigatório.");
            }
            if (funcionario.Endereço == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório.");
            }

            if (funcionario.Salario == 0)
            {
                throw new ArgumentException("Salário é obrigatório.");
            }
            if (funcionario.Cargo == string.Empty)
            {
                throw new ArgumentException("Cargo é obrigatório.");
            }
            if (funcionario.Admin == false && funcionario.Compras == false && funcionario.Empregado == false && funcionario.Financeiro == false && funcionario.Logistica == false && funcionario.RH == false && funcionario.Vendas == false)
            {
                throw new ArgumentException("Marque pelo menos uma permissão.");
            }
            if (funcionario.Usuario == string.Empty)
            {
                throw new ArgumentException("Usuário é obrigatório.");
            }

            if (funcionario.Senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatória.");
            }
            if (funcionario.Cargo == "Selecione o cargo")
            {
                throw new ArgumentException("Selecione um cargo valido");
            }
            string a = funcionario.Email;
            bool b = a.Contains("@");
            if (b == false)
            {
                throw new ArgumentException("Informe um email valido.");
            }
            

            db.Alterar(funcionario);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<FuncionarioDTO> Consultar(string nome)
        {

            return db.Consultar(nome);
        }

        public FuncionarioDTO Logar(string login, string senha)
        {
            if (login == string.Empty)
            {
                throw new ArgumentException("Usuário é obrigatório.");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatória.");
            }

            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Logar(login, senha);

        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();
        }
    }
}
