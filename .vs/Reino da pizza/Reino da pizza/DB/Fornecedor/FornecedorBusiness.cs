﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Fornecedor
{
    class FornecedorBusiness
    {

        FornecedorDatabase db = new FornecedorDatabase();

        public int Salvar(FornecedorDTO fornecedor)
        {
            if (fornecedor.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (fornecedor.CNPJ == string.Empty)
            {
                throw new ArgumentException("CNPJ é obrigatório.");
            }
            if (fornecedor.Endereco == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório.");
            }

            if (fornecedor.Telefone == string.Empty)
            {
                throw new ArgumentException("Contato é obrigatório.");
            }
            if (fornecedor.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório.");
            }

            if (fornecedor.Numero == string.Empty)
            {
                throw new ArgumentException("Número é obrigatório.");
            }
            if (fornecedor.Cidade == string.Empty)
            {
                throw new ArgumentException("Selecione um estado válido.");
            }
            string a = fornecedor.Email.ToString();
            bool b = a.Contains("@");
            bool c = a.Contains(".com");
            if (b == false && c == false)
            {
                throw new ArgumentException("Informe um email valido.");
            }
            return db.Salvar(fornecedor);
        }
        public void Alterar(FornecedorDTO fornecedor)
        {
            if (fornecedor.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (fornecedor.CNPJ == string.Empty)
            {
                throw new ArgumentException("CNPJ é obrigatório.");
            }
            if (fornecedor.Endereco == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório.");
            }

            if (fornecedor.Telefone == string.Empty)
            {
                throw new ArgumentException("Contato é obrigatório.");
            }
            if (fornecedor.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório.");
            }

            if (fornecedor.Numero == string.Empty)
            {
                throw new ArgumentException("Número é obrigatório.");
            }
            if (fornecedor.Cidade == "Selecione um estado")
            {
                throw new ArgumentException("Selecione um estado válido.");
            }


            string a = fornecedor.Email.ToString();
            bool b = a.Contains("@");
            bool c = a.Contains(".com");
            if (b == false || c == false)
            {
                throw new ArgumentException("Informe um email valido.");
            }
            db.Alterar(fornecedor);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<FornecedorDTO> Consultar()
        {

            return db.Consultar();
        }

        public List<FornecedorDTO> Listar()
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Listar();



        }
    }
}

