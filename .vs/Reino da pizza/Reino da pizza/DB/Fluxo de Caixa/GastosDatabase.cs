﻿using MySql.Data.MySqlClient;
using Reino_da_pizza.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Fluxo_de_Caixa
{
    class GastosDatabase
    {
        public int Salvar(GastosDTO gastos)
        {
            string script =
            @"INSERT INTO TB_GASTOS
            (
	            NM_GASTO,                                    
                DT_DATA,
                DS_TIPO,
                VL_VALOR
               
                
            )
            VALUES
            (
	            @NM_GASTO,                                    
                @DT_DATA,
                @DS_TIPO,
                @VL_VALOR
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_GASTO", gastos.Nome));
            parms.Add(new MySqlParameter("DS_TIPO", gastos.Tipo));
            parms.Add(new MySqlParameter("DT_DATA", gastos.Data));
            parms.Add(new MySqlParameter("VL_VALOR", gastos.Valor));



            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(GastosDTO gastos)
        {
            string script =
            @"UPDATE TB_PRODUTO
                 SET    NM_GASTO     =   @NM_GASTO,                              
                        DT_DATA      =   @DT_DATA,
                        DS_TIPO      =   @DS_TIPO,
                        VL_VALOR     =   @VL_VALOR
                        
               WHERE ID_GASTO = @ID_GASTO";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_GASTO", gastos.Id));
            parms.Add(new MySqlParameter("NM_GASTO", gastos.Nome));
            parms.Add(new MySqlParameter("DS_TIPO", gastos.Tipo));
            parms.Add(new MySqlParameter("DT_DATA", gastos.Data));
            parms.Add(new MySqlParameter("VL_VALOR", gastos.Valor));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_GASTOS WHERE ID_GASTO = @ID_GASTO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_GASTO", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<GastosDTO> Consultar(DateTime inicio, DateTime fim)
        {
            string script =
            @"SELECT * 
                FROM TB_GASTOS
                WHERE DT_DATA >= @inicio and DT_DATA <=@fim
               ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("inicio", inicio));
            parms.Add(new MySqlParameter("fim", fim));


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<GastosDTO> produto = new List<GastosDTO>();

            while (reader.Read())
            {
                GastosDTO novoProduto = new GastosDTO();
                novoProduto.Id = reader.GetInt32("ID_GASTO");
                novoProduto.Nome = reader.GetString("NM_GASTO");
                novoProduto.Valor = reader.GetDecimal("VL_VALOR");
                novoProduto.Tipo = reader.GetString("DS_TIPO");
                novoProduto.Data = reader.GetDateTime("DT_DATA");


                produto.Add(novoProduto);
            }
            reader.Close();

            return produto;
        }

        public List<GastosDTO> Listar()
        {
            string script = @"SELECT * FROM TB_PRODUTO
                               ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<GastosDTO> lista = new List<GastosDTO>();
            while (reader.Read())
            {

                GastosDTO novoProduto = new GastosDTO();
                novoProduto.Id = reader.GetInt32("ID_GASTO");
                novoProduto.Nome = reader.GetString("NM_GASTO");
                novoProduto.Valor = reader.GetDecimal("VL_VALOR");
                novoProduto.Tipo = reader.GetString("DS_TIPO");
                novoProduto.Data = reader.GetDateTime("DT_DATA");


                lista.Add(novoProduto);
            }
            reader.Close();

            return lista;



        }
    }
}
