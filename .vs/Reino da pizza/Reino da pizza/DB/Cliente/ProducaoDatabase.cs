﻿using MySql.Data.MySqlClient;
using Reino_da_pizza.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Cliente
{
    public class ProducaoDatabase
    {
        public int Salvar(ProducaoDTO cliente)
        {
            string script =
            @"INSERT INTO TB_PRODUCAO
            (
	            DS_DESCRICAO,                                    
                NM_PRODUCAO,
                VL_PRODUCAO,
                DS_FOTO
               
            )
            VALUES
            (
	            @DS_DESCRICAO,                                    
                @NM_PRODUCAO,
                @VL_PRODUCAO,
                @DS_FOTO
               
             
            )";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_DESCRICAO", cliente.Descricao));
            parms.Add(new MySqlParameter("NM_PRODUCAO", cliente.Nome));
            parms.Add(new MySqlParameter("VL_PRODUCAO", cliente.Valor));
            parms.Add(new MySqlParameter("DS_FOTO", cliente.Foto));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ProducaoDTO cliente)
        {
            string script =
            @"UPDATE TB_PRODUCAO
                 SET  DS_DESCRICAO  =  @DS_DESCRICAO,                                 
                      NM_PRODUCAO   =  @NM_PRODUCAO,
                      VL_PRODUCAO  =  @VL_PRODUCAO,
                      DS_FOTO       =  @DS_FOTO
               
                    WHERE ID_PRODUCAO = @ID_PRODUCAO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PRODUCAO", cliente.Id));
            parms.Add(new MySqlParameter("DS_DESCRICAO", cliente.Descricao));
            parms.Add(new MySqlParameter("NM_PRODUCAO", cliente.Nome));
            parms.Add(new MySqlParameter("VL_PRODUCAO", cliente.Valor));
            parms.Add(new MySqlParameter("DS_FOTO", cliente.Foto));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_PRODUCAO WHERE ID_PRODUCAO = @ID_PRODUCAO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PRODUCAO", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProducaoDTO> Consultar()
        {
            string script =
            @"SELECT * 
                FROM TB_PRODUCAO
              ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProducaoDTO> cliente = new List<ProducaoDTO>();

            while (reader.Read())
            {
                ProducaoDTO novoCliente = new ProducaoDTO();
                novoCliente.Id = reader.GetInt32("ID_PRODUCAO");
                novoCliente.Nome = reader.GetString("NM_PRODUCAO");
                novoCliente.Valor = reader.GetDecimal("VL_PRODUCAO");
                //novoCliente.Foto = reader.GetString("DS_FOTO");
                novoCliente.Descricao = reader.GetString("DS_DESCRICAO");

                cliente.Add(novoCliente);
            }
            reader.Close();

            return cliente;
        }
        public List<ProducaoDTO> Listar()
        {
            string script =
            @"SELECT * 
                FROM TB_PRODUCAO
               ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProducaoDTO> cliente = new List<ProducaoDTO>();

            while (reader.Read())
            {
                ProducaoDTO novoCliente = new ProducaoDTO();
                novoCliente.Id = reader.GetInt32("ID_PRODUCAO");
                novoCliente.Nome = reader.GetString("NM_PRODUCAO");
                novoCliente.Valor = reader.GetDecimal("VL_PRODUCAO");
                //novoCliente.Foto = reader.GetString("DS_FOTO");
                novoCliente.Descricao = reader.GetString("DS_DESCRICAO");

                cliente.Add(novoCliente);
            }
            reader.Close();

            return cliente;
        }
    }
}
