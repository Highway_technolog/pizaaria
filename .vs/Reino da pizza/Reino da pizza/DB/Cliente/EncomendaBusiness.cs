﻿using Reino_da_pizza.DB.Estoque;
using Reino_da_pizza.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Reino_da_pizza.DB.Cliente
{
    class EncomendaBusiness
    {
        EncomendaDatabase db = new EncomendaDatabase();



        public void ValidarEstoque(List<ProducaoDTO> produtos)
        {
            EstoqueBusiness business = new EstoqueBusiness();
            EstoqueDTO estoque = new EstoqueDTO();

            if (estoque.IdProduto != 1)
            {
                var lista = produtos.GroupBy(x => new { x.Id, x.Nome });
                foreach (var item in lista)
                {
                    estoque = business.ConsultarNome(item.Key.Id);
                    if (item.Count() > estoque.Quantidade)
                    {
                        throw new ArgumentException($"produto {item.Key.Nome} em falta.");
                    }
                }
            }
        }


        public int Salvar(EncomendaDTO pedido, List<ProducaoDTO> produtos)
        {



            if (pedido.Entrega == "Selecione")
            {
                throw new ArgumentException("Selecione a forma de retirada.");
            }

            if (pedido.Pagamento == "Selecione")
            {
                throw new ArgumentException("Selecione a forma de pagamento.");
            }


            EncomendaDatabase pedidoDatabase = new EncomendaDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            ItensEncomendaBusiness itemBusiness = new ItensEncomendaBusiness();
            foreach (ProducaoDTO item in produtos)
            {


                ItensEncomendaDTO itemDto = new ItensEncomendaDTO();
                itemDto.IdProducao = item.Id;
                itemDto.IdEncomenda = idPedido;

                itemBusiness.Salvar(itemDto);
            }

            return idPedido;

        }


        public void Alterar(int id)
        {
            db.Alterar(id);
        }





        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<EncomendaDTO> Consultar(DateTime inicio, DateTime fim)
        {
            return db.Consultar(inicio, fim);
        }

        public List<EncomendaDTO> ConsultarStatus()
        {
            return db.ConsultarStatus();
        }
       



    }
}
