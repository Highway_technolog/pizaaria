﻿using MySql.Data.MySqlClient;
using Reino_da_pizza.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Cliente
{
    class ClienteEventualDatabase
    {
        public int Salvar(ClienteEventualDTO cliente)
        {
            string script =
            @"INSERT INTO TB_CLIENTEEVENTUAL
            (
	            NM_NOME,                                    
                
                DS_ENDERECO,
                DS_NUMERO,
                DS_CEP,
               
                DS_COMPLEMENTO,
                DS_TELEFONE
              
            )

            VALUES
            (
	            @NM_NOME,
              
                @DS_ENDERECO,
                @DS_NUMERO,
                @DS_CEP,
             
                @DS_COMPLEMENTO,
                @DS_TELEFONE
            

            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_NOME", cliente.Nome));

            parms.Add(new MySqlParameter("DS_ENDERECO", cliente.Endereço));
            parms.Add(new MySqlParameter("DS_NUMERO", cliente.Numero));
            parms.Add(new MySqlParameter("DS_CEP", cliente.CEP));

            parms.Add(new MySqlParameter("DS_COMPLEMENTO", cliente.Complemento));
            parms.Add(new MySqlParameter("DS_TELEFONE", cliente.Telefone));



            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ClienteEventualDTO cliente)
        {
            string script =
            @"UPDATE TB_CLIENTEEVENTUAL
                 SET 

                     ID_CLIENTEEVENTUAL      =@ID_CLIENTEEVENTUAL,
                     NM_NOME         = @NM_NOME,
	               
	                 DS_ENDERECO     = @DS_ENDERECO,
	                 DS_CEP          = @DS_CEP,
	                 DS_NUMERO       = @DS_NUMERO,
	              
                     DS_COMPLEMENTO  = @DS_COMPLEMENTO,
                    
                     DS_TELEFONE     = @DS_TELEFONE
                     
               WHERE ID_CLIENTEEVENTUAL = @ID_CLIENTEEVENTUAL";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_CLIENTEEVENTUAL", cliente.ID));
            parms.Add(new MySqlParameter("NM_NOME", cliente.Nome));

            parms.Add(new MySqlParameter("DS_ENDERECO", cliente.Endereço));
            parms.Add(new MySqlParameter("DS_NUMERO", cliente.Numero));
            parms.Add(new MySqlParameter("DS_CEP", cliente.CEP));

            parms.Add(new MySqlParameter("DS_TELEFONE", cliente.Telefone));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_CLIENTEEVENTUAL WHERE ID_CLIENTEEVENTUAL = @ID_CLIENTEEVENTUAL";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ClienteEventualDTO> Consultar(string Nome)
        {
            string script =
            @"SELECT * 
                FROM TB_CLIENTEEVENTUAL
               WHERE NM_NOME LIKE @NM_NOME";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_NOME", "%" + Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteEventualDTO> cliente = new List<ClienteEventualDTO>();

            while (reader.Read())
            {
                ClienteEventualDTO novoCliente = new ClienteEventualDTO();
                novoCliente.ID = reader.GetInt32("ID_CLIENTE");
                novoCliente.Nome = reader.GetString("NM_NOME");

                novoCliente.CEP = reader.GetString("DS_CEP");
                novoCliente.Numero = reader.GetString("DS_NUMERO");

                novoCliente.Complemento = reader.GetString("DS_COMPLEMENTO");
                novoCliente.Telefone = reader.GetString("DS_TELEFONE");

                cliente.Add(novoCliente);
            }
            reader.Close();

            return cliente;
        }
        public List<ClienteEventualDTO> Listar()
        {
            string script =
            @"SELECT * 
                FROM TB_CLIENTEEVENTUAL;";


            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteEventualDTO> cliente = new List<ClienteEventualDTO>();

            while (reader.Read())
            {
                ClienteEventualDTO novoCliente = new ClienteEventualDTO();
                novoCliente.ID = reader.GetInt32("ID_CLIENTE");
                novoCliente.Nome = reader.GetString("NM_NOME");
                novoCliente.Endereço = reader.GetString("NM_ENDERECO");
                novoCliente.CEP = reader.GetString("DS_CEP");
                novoCliente.Numero = reader.GetString("DS_NUMERO");
                novoCliente.Complemento = reader.GetString("DS_COMPLEMENTO");
                novoCliente.Telefone = reader.GetString("DS_TELEFONE");



                cliente.Add(novoCliente);
            }
            reader.Close();

            return cliente;
        }
    }
}
