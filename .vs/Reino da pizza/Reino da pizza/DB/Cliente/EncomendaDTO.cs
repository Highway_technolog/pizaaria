﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Cliente
{
    public class EncomendaDTO
    {
        public int ID { get; set; }
        public DateTime Data { get; set; }
        public decimal Valor { get; set; }
        public int IdCliente { get; set; }
        public int IdClienteEventual { get; set; }
        public string Cliente { get; set; }
        public string Pagamento { get; set; }
        public string Entrega { get; set; }
        public string Status { get; set; }
    }
}
