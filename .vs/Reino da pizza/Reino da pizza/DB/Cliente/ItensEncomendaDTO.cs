﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Cliente
{
    public class ItensEncomendaDTO
    {
        public int IdItensEncomenda { get; set; }
        public int IdProducao { get; set; }
        public int IdEncomenda { get; set; }
    }
}
