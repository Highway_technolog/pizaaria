﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Cliente
{
    class ProducaoBusiness
    {
        ProducaoDatabase db = new ProducaoDatabase();

        public int Salvar(ProducaoDTO pro)
        {
            return db.Salvar(pro);
        }

        public void Alterar(ProducaoDTO pro)
        {
            db.Alterar(pro);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<ProducaoDTO> Consultar()
        {
            return db.Consultar();
        }
        public List<ProducaoDTO> Listar()
        {
            return db.Listar();
        }
    }
}


