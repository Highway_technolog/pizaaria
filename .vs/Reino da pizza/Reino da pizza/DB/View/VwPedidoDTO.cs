﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.View
{
    public class VwPedidoDTO
    {
        public DateTime Data { get; set; }
        public decimal Valor { get; set; }
        public string Fornecedor { get; set; }
        public string Produto { get; set; }
    }
}
