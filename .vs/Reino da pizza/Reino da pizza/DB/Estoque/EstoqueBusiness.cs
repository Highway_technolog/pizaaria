﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Estoque
{
    class EstoqueBusiness
    {
        EstoqueDatabase db = new EstoqueDatabase();

        public EstoqueDTO ConsultarNome(int id)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.ConsultarNome(id);
        }
        public int Salvar(EstoqueDTO estoque)
        {
            return db.Salvar(estoque);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }
        public void RemoverFornecedor(int id)
        {
            db.RemoverFornecedor(id);
        }
        public List<EstoqueDTO> Consultar()
        {
            return db.Consultar();
        }
        public void RemoverItem(EstoqueDTO estoque)
        {
            db.RemoverItem(estoque);
        }
        public void AdicionarItem(int qtd, int idpedido, DateTime data)
        {
            db.AdicionarItem(qtd, idpedido, data);
        }
        public List<EstoqueDTO> ConsultarPorNome(string nome)
        {
            return db.ConsultarPorNome(nome);
        }

    }
}
