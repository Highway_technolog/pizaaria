﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reino_da_pizza.DB.Ponto
{
    class PontoDTO
    {
        public int Id { get; set; }
        public int IdFuncionario { get; set; }
        public DateTime Entrada { get; set; }
        public DateTime AlmocoSaida { get; set; }
        public DateTime AlmocoVolta { get; set; }
        public DateTime Saida { get; set; }
    }
}
