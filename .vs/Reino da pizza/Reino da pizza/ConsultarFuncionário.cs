﻿using Reino_da_pizza.DB.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reino_da_pizza
{
    public partial class ConsultarFuncionário : Form
    {
        public ConsultarFuncionário()
        {
            InitializeComponent();
        }

        public void CarregarGrid()
        {
            string nome = txtNome.Text;
            FuncionarioBusiness Business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = Business.Consultar(nome);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CarregarGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Error);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 12)
            {
                FuncionarioDTO funcionario = dataGridView1.CurrentRow.DataBoundItem as FuncionarioDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o funcionário?", "Panetteria Fiorentino",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(funcionario.ID);

                    CarregarGrid();
                }
            }
            if (e.ColumnIndex == 13)
            {
                FuncionarioDTO funcionario = dataGridView1.CurrentRow.DataBoundItem as FuncionarioDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o funcionário?", "Panetteria Fiorentino",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(funcionario.ID);

                    CarregarGrid();
                }
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label17_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }
    }
}
