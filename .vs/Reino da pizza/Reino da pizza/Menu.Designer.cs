﻿namespace Reino_da_pizza
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.label10 = new System.Windows.Forms.Label();
            this.ptbFornecedor = new System.Windows.Forms.PictureBox();
            this.lblus = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ptbProduto = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.ptbFolhaPagamento = new System.Windows.Forms.PictureBox();
            this.ptbCliente = new System.Windows.Forms.PictureBox();
            this.CMSFornecedor = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.CMSCliente = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.CMSFolhaPagamento = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.CMSPedidoVenda = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.CMSFuncionario = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.CMSProduto = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ptbFornecedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbFolhaPagamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCliente)).BeginInit();
            this.CMSFornecedor.SuspendLayout();
            this.CMSCliente.SuspendLayout();
            this.CMSFolhaPagamento.SuspendLayout();
            this.CMSPedidoVenda.SuspendLayout();
            this.CMSFuncionario.SuspendLayout();
            this.CMSProduto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(637, 318);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 18);
            this.label10.TabIndex = 229;
            this.label10.Text = "Fornecedor";
            // 
            // ptbFornecedor
            // 
            this.ptbFornecedor.BackColor = System.Drawing.Color.Transparent;
            this.ptbFornecedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbFornecedor.BackgroundImage")));
            this.ptbFornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbFornecedor.ContextMenuStrip = this.CMSFornecedor;
            this.ptbFornecedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptbFornecedor.Location = new System.Drawing.Point(630, 244);
            this.ptbFornecedor.Name = "ptbFornecedor";
            this.ptbFornecedor.Size = new System.Drawing.Size(98, 71);
            this.ptbFornecedor.TabIndex = 228;
            this.ptbFornecedor.TabStop = false;
            // 
            // lblus
            // 
            this.lblus.AutoSize = true;
            this.lblus.BackColor = System.Drawing.Color.Maroon;
            this.lblus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblus.ForeColor = System.Drawing.Color.White;
            this.lblus.Location = new System.Drawing.Point(73, 22);
            this.lblus.Name = "lblus";
            this.lblus.Size = new System.Drawing.Size(64, 20);
            this.lblus.TabIndex = 227;
            this.lblus.Text = "Usuário";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Maroon;
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Location = new System.Drawing.Point(12, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(55, 50);
            this.pictureBox3.TabIndex = 226;
            this.pictureBox3.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(643, 440);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 18);
            this.label6.TabIndex = 225;
            this.label6.Text = "Produto";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(7, 318);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 18);
            this.label5.TabIndex = 224;
            this.label5.Text = "Folha de pagamento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(30, 440);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 18);
            this.label4.TabIndex = 223;
            this.label4.Text = "Pedido venda";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(636, 210);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 18);
            this.label3.TabIndex = 222;
            this.label3.Text = "Funcionário";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(49, 210);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 18);
            this.label2.TabIndex = 221;
            this.label2.Text = "Cliente";
            // 
            // ptbProduto
            // 
            this.ptbProduto.BackColor = System.Drawing.Color.Transparent;
            this.ptbProduto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbProduto.BackgroundImage")));
            this.ptbProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbProduto.ContextMenuStrip = this.CMSProduto;
            this.ptbProduto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptbProduto.Location = new System.Drawing.Point(630, 359);
            this.ptbProduto.Name = "ptbProduto";
            this.ptbProduto.Size = new System.Drawing.Size(98, 71);
            this.ptbProduto.TabIndex = 220;
            this.ptbProduto.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox11.BackgroundImage")));
            this.pictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox11.ContextMenuStrip = this.CMSFuncionario;
            this.pictureBox11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox11.Location = new System.Drawing.Point(628, 136);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(100, 71);
            this.pictureBox11.TabIndex = 219;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox10.BackgroundImage")));
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox10.ContextMenuStrip = this.CMSPedidoVenda;
            this.pictureBox10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox10.Location = new System.Drawing.Point(36, 359);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(98, 71);
            this.pictureBox10.TabIndex = 218;
            this.pictureBox10.TabStop = false;
            // 
            // ptbFolhaPagamento
            // 
            this.ptbFolhaPagamento.BackColor = System.Drawing.Color.Transparent;
            this.ptbFolhaPagamento.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbFolhaPagamento.BackgroundImage")));
            this.ptbFolhaPagamento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbFolhaPagamento.ContextMenuStrip = this.CMSFolhaPagamento;
            this.ptbFolhaPagamento.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptbFolhaPagamento.Location = new System.Drawing.Point(34, 244);
            this.ptbFolhaPagamento.Name = "ptbFolhaPagamento";
            this.ptbFolhaPagamento.Size = new System.Drawing.Size(98, 71);
            this.ptbFolhaPagamento.TabIndex = 217;
            this.ptbFolhaPagamento.TabStop = false;
            // 
            // ptbCliente
            // 
            this.ptbCliente.BackColor = System.Drawing.Color.Transparent;
            this.ptbCliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbCliente.BackgroundImage")));
            this.ptbCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbCliente.ContextMenuStrip = this.CMSCliente;
            this.ptbCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptbCliente.Location = new System.Drawing.Point(32, 136);
            this.ptbCliente.Name = "ptbCliente";
            this.ptbCliente.Size = new System.Drawing.Size(100, 71);
            this.ptbCliente.TabIndex = 216;
            this.ptbCliente.TabStop = false;
            // 
            // CMSFornecedor
            // 
            this.CMSFornecedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSFornecedor.BackgroundImage")));
            this.CMSFornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSFornecedor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.CMSFornecedor.Name = "CMSProduto";
            this.CMSFornecedor.Size = new System.Drawing.Size(126, 48);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem5.Image")));
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem5.Text = "Novo";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem6.Image")));
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem6.Text = "Consultar";
            // 
            // CMSCliente
            // 
            this.CMSCliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSCliente.BackgroundImage")));
            this.CMSCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSCliente.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem7,
            this.toolStripMenuItem8});
            this.CMSCliente.Name = "CMSProduto";
            this.CMSCliente.Size = new System.Drawing.Size(126, 48);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem7.Image")));
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem7.Text = "Novo";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem8.Image")));
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem8.Text = "Consultar";
            // 
            // CMSFolhaPagamento
            // 
            this.CMSFolhaPagamento.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSFolhaPagamento.BackgroundImage")));
            this.CMSFolhaPagamento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSFolhaPagamento.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem9,
            this.toolStripMenuItem10});
            this.CMSFolhaPagamento.Name = "CMSProduto";
            this.CMSFolhaPagamento.Size = new System.Drawing.Size(126, 48);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem9.Image")));
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem9.Text = "Novo";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem10.Image")));
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem10.Text = "Consultar";
            // 
            // CMSPedidoVenda
            // 
            this.CMSPedidoVenda.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSPedidoVenda.BackgroundImage")));
            this.CMSPedidoVenda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSPedidoVenda.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem11,
            this.toolStripMenuItem12});
            this.CMSPedidoVenda.Name = "CMSProduto";
            this.CMSPedidoVenda.Size = new System.Drawing.Size(126, 48);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem11.Image")));
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem11.Text = "Novo";
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem12.Image")));
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem12.Text = "Consultar";
            // 
            // CMSFuncionario
            // 
            this.CMSFuncionario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSFuncionario.BackgroundImage")));
            this.CMSFuncionario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSFuncionario.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem13,
            this.toolStripMenuItem14});
            this.CMSFuncionario.Name = "CMSProduto";
            this.CMSFuncionario.Size = new System.Drawing.Size(126, 48);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem13.Image")));
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem13.Text = "Novo";
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem14.Image")));
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem14.Text = "Consultar";
            // 
            // CMSProduto
            // 
            this.CMSProduto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSProduto.BackgroundImage")));
            this.CMSProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSProduto.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.CMSProduto.Name = "CMSCliente";
            this.CMSProduto.Size = new System.Drawing.Size(126, 48);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem3.Image")));
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem3.Text = "Novo";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem4.Image")));
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(125, 22);
            this.toolStripMenuItem4.Text = "Consultar";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Orange;
            this.pictureBox2.Location = new System.Drawing.Point(-10, 65);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(810, 13);
            this.pictureBox2.TabIndex = 244;
            this.pictureBox2.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Maroon;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Gold;
            this.label8.Location = new System.Drawing.Point(321, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 42);
            this.label8.TabIndex = 245;
            this.label8.Text = "Menu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Maroon;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gold;
            this.label1.Location = new System.Drawing.Point(692, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 25);
            this.label1.TabIndex = 250;
            this.label1.Text = "-";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Maroon;
            this.pictureBox1.Location = new System.Drawing.Point(-5, -10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(805, 76);
            this.pictureBox1.TabIndex = 246;
            this.pictureBox1.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Maroon;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Gold;
            this.label7.Location = new System.Drawing.Point(710, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 18);
            this.label7.TabIndex = 249;
            this.label7.Text = "X";
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 517);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblus);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.ptbFornecedor);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ptbProduto);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.ptbFolhaPagamento);
            this.Controls.Add(this.ptbCliente);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Menu";
            this.Text = "Menu";
            ((System.ComponentModel.ISupportInitialize)(this.ptbFornecedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbFolhaPagamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCliente)).EndInit();
            this.CMSFornecedor.ResumeLayout(false);
            this.CMSCliente.ResumeLayout(false);
            this.CMSFolhaPagamento.ResumeLayout(false);
            this.CMSPedidoVenda.ResumeLayout(false);
            this.CMSFuncionario.ResumeLayout(false);
            this.CMSProduto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox ptbFornecedor;
        private System.Windows.Forms.Label lblus;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox ptbProduto;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox ptbFolhaPagamento;
        private System.Windows.Forms.PictureBox ptbCliente;
        private System.Windows.Forms.ContextMenuStrip CMSFornecedor;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ContextMenuStrip CMSCliente;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ContextMenuStrip CMSFolhaPagamento;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ContextMenuStrip CMSPedidoVenda;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ContextMenuStrip CMSFuncionario;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ContextMenuStrip CMSProduto;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label7;
    }
}